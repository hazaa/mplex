# MPLEX

This repository contains:

*   Tools for maximum entropy network models and multiplex networks in Python (see [measure.py](measure.py)).
*   Application to regional transport dataset of existing algorithms in the literature (see repository https://gitlab.com/hazaa/cm_ml).
    Some **original code** developed for the articles :
    
    *   A.Hazan, "A network model of freight data with spatial dependence", submitted. 
        *   code:  notebooks [region_freight_basic_stats.ipynb](region_freight_basic_stats.ipynb), [region_freight_maxent.ipynb](region_freight_maxent.ipynb)
        *   dataset: see paper.

References:

*   [Gemmetto 18] "On metrics and models for multiplex networks" http://hdl.handle.net/1887/61132
*   see refs in https://gitlab.com/hazaa/cm_ml 

# Installation

Dependences:

*  python3, jupyter-notebook, numpy, matplolib, igraph
*  https://gitlab.com/hazaa/cm_ml, (https://gitlab.com/hazaa/scitools for plotting)

# Usage

see jupyter notebooks (.ipynb)


# Multiplex datasets 

* https://comunelab.fbk.eu/data.php
* https://github.com/tak-wah/pymnet_MultilayerNetwork    (cancer)
* https://github.com/r-duh/MultiplexDiseasomeCommunities 
* http://deim.urv.cat/~alephsys/data.html
* https://sites.google.com/site/muratalaboratory/home/datasets
