# -*- coding: utf-8 -*-
"""
SURVEY-BASED SUB-NATIONAL IO/FREIGHT modelling with MAXENT 

FREIGHT/PASSENGER
etisplus (http://www.etisplus.eu/, http://viewer.etisplus.net/)
         (https://web.archive.org/web/20161012142330/http://www.etisplus.eu/data/Public/Downloads.aspx)
         (https://ftp.demis.nl/Outgoing/ETISplus/DataDeliverables/TextFiles/etis_2005_observed.zip)
         DATA: https://ftp.demis.nl/outgoing/etisplus/datadeliverables/TextFiles/
         REPORTS: https://www.tmleuven.be/en/project/etisplus
         NB: O/D matrix is modelled.
         used by: http://www.high-tool.eu/, 
                 http://www.transtools3.eu/system/files/TT3_WP5_D5.2_Data%20description.pdf
                 ftp://ftp.jrc.es/users/transtools/public/Documentation/
Commodity Flow Survey (CFS)  2009 sweden https://www.trafa.se/en/travel-survey/commodity-flows/
        NB: pas de origin/destination matrix
"ECHO"  [Envoi – CHargeurs – Opérateurs] 2004 france
        https://www.ifsttar.fr/ressources-en-ligne/espace-science-et-societe/territoires/dossiers-thematiques/quelle-logistique-urbaine-dans-le-futur/chaines-logistiques-urbaines/presentation-de-lenquete-echo/
SITRAM https://www.statistiques.developpement-durable.gouv.fr/donnees-sur-les-flux-de-marchandises-sitram-annee-2015
        NB: pas de origin/destination matrix
"""

"""
TODO:
    Q: harmonised, observed graphs are dense ?
    Q: _harmonised: road : comment la OD est obtenue ? (not present in 'observed')
    
    compare freight survey (etisplus) and mrio non-survey (euregio)
    maxent: compare survey and MaxEnt models
            couterfactual: show china mrio (GM-based) modelled with EGM. should fit! 
            barth18 cite https://arxiv.org/abs/1407.6297, https://doi.org/10.1073/pnas.1018962108
                    (proba fitté)
                    utilise ce null model pour community detec.
            halu13, EGM : modèles proches, x^\alpha
                    Q: qui cite Halu, EGM ?
    layer: etisplus: freight (road/rail/sea); passengers

           freight: compare layers with/without transshipment (see Thissen)
    time-dep: etisplus : {2005, 2010}
              model: activity-driven ? 
    city-level : ETIS: mdb:  'TrafficCountPass', 'TrafficCountFreight'
                 filtering + ? (no GM modelling ).
                        (NB: Barth18 étudie le graph "sparsening")
                 aggregation/density:  estimate density of network    
                 parafac
                 validation: données Japon firms: contacter Mizuno
"""

from cfg import *
import pandas as pd
from igraph import *
import networkx as nx
import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt
from matplotlib import cm 
import shapely.wkt
import warnings
import powerlaw 
import fiona
from shapely.geometry import Point, shape, MultiPolygon
from descartes.patch import PolygonPatch
from shapely.ops import unary_union

from tools import convert_ig_to_nx
#import cm_ml.cm as cm_cy
from cm_ml.cmtools import knn_avg_UBCM,snn
#from cm_ml.dataset import *
try: from cm_ml.dataset import distance_matrix_geo # for get_city_distance
except: print('cm_ml.dataset.distance_matrix_geo could not be loaded') 

#------------------------------------
# HELPER FUNC
def pd_to_ig(df, edge_attrs=['weight'],directed=True):
    """
    Refs:
    https://igraph.org/python/doc/igraph.Graph-class.html#TupleList
    """
    edge_set = df.to_numpy().tolist()
    # add edge set
    g=Graph.TupleList(edge_set, directed=directed, edge_attrs=edge_attrs)
    # check
    if np.any(np.array(g.is_loop())): warnings.warn('has loop')
    if np.any(np.array(g.is_multiple())): warnings.warn('has multiple')
    return g  
#------------------------------------
# LOAD DATA

def load_etisplus(year='2010',geodata=True,tra_type='all', data_src='harmonised',
                 directed=True):
    """
    
    (copied from mplexio.read_ots_yrp_data_igraph)
    
    input:
    -----
    year: int
    year. Possible values: '2005','2010'    

    geo: boolean 
    read and return geographic data

    tra_type: str
    transport type. Possible values : 'all', 'train', 'sea', 'road'

    data_src: str
    data source. Possible values: 'harmonised', 'observed'   

    output:
    ------
    mplex: dict
    igraph objects for 'rail','sea','road'. 'geodata' is a geopandas object
    
    """
    if not year in ['2005','2010']:   raise ValueError('invalid year')         
    if data_src in ['observed','harmonised']:
        PATH_RAIL =  "{}/ETISplus/etis_{}_{}/f_estat_eu27_rail_regional.csv".format(ROOT,year,data_src) 
        PATH_SEA =  "{}/ETISplus/etis_{}_{}/f_estat_eu27_iww_regional.csv".format(ROOT,year,data_src) 
        df_rail = pd.read_csv(PATH_RAIL)
        df_sea = pd.read_csv(PATH_SEA)
        # rail
        #'f_estat_eu27_rail_regional' = "Total railway freight transport from region to region"
        # see __Tables.csv
        df_rail = df_rail[ ['ORIGINZONE_2_ID', 'DEST_ZONE_2_ID',
                     'f_estat_eu27_rail_regional']]
        edge_set_rail = df_rail.to_numpy().tolist()
        g_rail=Graph.TupleList(edge_set_rail, directed=directed, weights=True)
        # sea 
        df_sea = df_sea[ ['ORIGINZONE_2_ID', 'DEST_ZONE_2_ID',
                     'f_estat_eu27_iww_regional_tonnes', 'NST07_ID']]
        edge_set_sea = df_sea.to_numpy().tolist()
        g_sea=Graph.TupleList(edge_set_sea, directed=directed,
                                edge_attrs=['weight','NST07_ID'])
        # check loops, multiple                        
        for g in [g_sea,g_rail]:
            if np.any(np.array(g.is_loop())): 
                loop=True;  warnings.warn('has loop')
            if np.any(np.array(g.is_multiple())): 
                multiple=True; warnings.warn('has multiple')
            if loop or multiple:    
                g=g.simplify(multiple=True, loops=True, combine_edges=dict(weight="sum"))
        if geodata:
            """ 
            https://gis.stackexchange.com/questions/174159/convert-a-pandas-dataframe-to-a-geodataframe
            https://shapely.readthedocs.io/en/latest/manual.html#MultiPolygon
            """
            PATH_GEODATA =  "{}/ETISplus/etis_{}_{}/_EZ2006_2.csv".format(ROOT,year,data_src) 
            gp = gpd.read_file(PATH_GEODATA)
            #gp = gp.set_geometry('SHAPE')  # FAILS
            gp['geometry']=gp['SHAPE'].map(shapely.wkt.loads)
            gp['CENTROID']=gp['CENTROID'].map(shapely.wkt.loads)
            gp = gp.drop('SHAPE',axis=1) 
            gp = gp.astype({'ID': 'int64'})
            #gp.plot(); plt.show()
            return { 'rail':g_rail,'sea':g_sea,'geodata':gp }
        else :                           
            return { 'rail':g_rail,'sea':g_sea }
    else: raise ValueError('invalid data_src')      

def get_distance_matrix(d,tra_type='rail'):
    """
    compute the distance matrix
    
    Usage:
    ------
    >>d = load_etisplus(year='2010',geodata=True, data_src='observed', directed=True)
    >>D = get_distance_matrix(d,tra_type='rail')

    input:
    -----
    mplex: dict 
    the output of load_etisplus

    tra_type: str
    transport type. Possible values : 'all', 'train', 'sea', 'road'
    
    output:
    ------
    D: array, shape=(n,n)
    the distance matrix
    """
    df = pd.DataFrame( {'ID': d[tra_type].vs['name']})
    # join geo/rail
    df_merge = pd.merge(df,d['geodata'] )
    #df_merge.dtypes
    coord = np.array(df_merge['CENTROID'].apply( lambda p: [p.x,p.y] ).to_list())
    return distance_matrix_geo(coord,coord)

# --------------------------------
# BASIC STATS

def choropleth_network():
    """ choropleth map of network measures
        e.g. knn, plot regions st knn < K
        TODO:
        try with plotly dash, for interactivity (select,labels, links...)
    """
    # load etis
    d = load_etisplus(year='2010',geodata=True, data_src='observed', directed=False)
    g = d['rail']
    A = g.get_adjacency(type=GET_ADJACENCY_BOTH, attribute=None, default=0, eids=False)
    A = np.array(A.data)
    if np.any(A>1): 
        warnings.warn('multiple edge')
        A=np.clip(A,0,1)
    W = g.get_adjacency(type=GET_ADJACENCY_BOTH, attribute='weight', default=0, eids=False)
    W = np.array(W.data)
    s = np.array(g.strength(weights="weight"))
    k=np.array(g.degree())
    # network metrics
    knn_= knn_avg_UBCM(A,k)
    snn_ = snn(A,k,s)
    # join geo/rail
    df = pd.DataFrame( {'ID': g.vs['name']})
    df_merge = pd.merge(df,d['geodata'] ) # TODO: gpd.merge ?
    #df_EU = df_merge[['Name','geometry']]
    df_EU = df_merge[['ID','geometry']]
    gdf_EU = gpd.geodataframe.GeoDataFrame(df_EU)
    # select
    Kinf=0; Ksup = 40;  idx = knn_ < Ksup
    Kinf=40; Ksup = 140;  idx = (Kinf< knn_)* (knn_ < Ksup)
    gdf_select= gdf_EU.iloc[idx]
    # TODO: add other regions in EU in gdf_EU and in knn_
    # plot
    # https://geopandas.org/mapping.html
    #fig, (ax1,ax2,ax3) = plt.subplots(3, 1)
    fig1, ax1 = plt.subplots(1, 1)
    fig2, (ax2,ax3,ax4) = plt.subplots(3, 1)
    gdf_EU.plot(knn_,ax=ax1, legend=True)
    gdf_EU.plot(k,ax=ax4, legend=True)
    #plt.show()
    gdf_select.plot(ax=ax2)
    ax3.scatter(k,knn_,s=5,c='r')
    ax3.plot([0,k.max()],[Ksup,Ksup],'r--')
    ax3.set_xlabel('$k$');ax3.set_ylabel('$k^{nn}$')
    # plot network    df_EU = df_merge[['ID','CENTROID']]
    fig1.show()
    fig2.show()

def choropleth_network_with_nodes():    
    """
    NOT USED
    """
    gdf_EU = gpd.geodataframe.GeoDataFrame(df_EU)
    node_cmap=plt.cm.hot
    viridis = cm.get_cmap('viridis', 12)
    G = convert_ig_to_nx(g)
    pos = {}
    for i,n in enumerate(G.nodes): 
        name_ig = g.vs['name'][i]  
        idx=d['geodata'].index.map( d['geodata']['ID']==name_ig)
        df_= d['geodata'].iloc[idx]
        lat_ = df_['CENTROID'].iloc[0].x
        lon_ = df_['CENTROID'].iloc[0].y
        pos[n] = (lat_,lon_) 
    #lab = nx.draw_networkx_labels(G,pos,labels=labels, ax=ax1)
    #edges = nx.draw_networkx_edges(G, pos, ax=ax1, alpha=0.5)
    #node_color=viridis(mystat)[:,0:3]
    #nodes = nx.draw_networkx_nodes(G, pos, node_color=node_color,node_size=500,ax=ax_map)
    
def powerlaw_stats():
    """
    http://nbviewer.ipython.org/github/jeffalstott/powerlaw/blob/master/manuscript/Manuscript_Code.ipynb
    """
    # load etis
    d = load_etisplus(year='2010',geodata=True, data_src='observed', directed=False)
    g = d['rail']
    A = g.get_adjacency(type=GET_ADJACENCY_BOTH, attribute=None, default=0, eids=False)
    A = np.array(A.data)
    if np.any(A>1): 
        warnings.warn('multiple edge')
        A=np.clip(A,0,1)
    W = g.get_adjacency(type=GET_ADJACENCY_BOTH, attribute='weight', default=0, eids=False)
    W = np.array(W.data)
    s = np.array(g.strength(weights="weight"))
    k=np.array(g.degree())
    #### k
    data=k
    fit = powerlaw.Fit(data, discrete=True,xmin=np.min(k))
    fit.distribution_compare('power_law', 'lognormal', 'exponential')
    fig = fit.plot_ccdf(linewidth=3, label='Empirical Data')
    fit.power_law.plot_ccdf(ax=fig, color='r', linestyle='--', label='Power law fit')
    fit.lognormal.plot_ccdf(ax=fig, color='g', linestyle='--', label='Lognormal fit')
    fit.exponential.plot_ccdf(ax=fig, color='k', linestyle='--', label='exp fit')
    fig.set_ylabel(u"p(X≥x)")
    fig.set_xlabel("k")
    #setp(gca().get_xticklabels(), visible=True)
    handles, labels = fig.get_legend_handles_labels()
    fig.legend(handles, labels, loc=3)
    plt.show()
    #figname = 'FigLognormal'
    #savefig(figname+'.eps', bbox_inches='tight')
    #savefig(figname+'.tiff', bbox_inches='tight', dpi=300)
    #### s
    data=s
    fit = powerlaw.Fit(data, discrete=False,xmin=np.min(k))
    fit.distribution_compare('power_law', 'lognormal', 'exponential')
    fig = fit.plot_ccdf(linewidth=3, label='Empirical Data')
    fit.power_law.plot_ccdf(ax=fig, color='r', linestyle='--', label='Power law fit')
    fit.lognormal.plot_ccdf(ax=fig, color='g', linestyle='--', label='Lognormal fit')
    fit.exponential.plot_ccdf(ax=fig, color='k', linestyle='--', label='exp fit')
    fig.set_ylabel(u"p(X≥x)")
    fig.set_xlabel("s")
    #setp(gca().get_xticklabels(), visible=True)
    handles, labels = fig.get_legend_handles_labels()
    fig.legend(handles, labels, loc=3)
    plt.show()
# --------------------------------
# CONTROL: IMPEDANCE
def read_group_impedance_EZ2006_2(year="2010",tra_type='rail',path="{}/ETISplus/etis_{}_observed/p_imp_{}.csv"):
    """
    read impedance file (given at level EZ2006_3),
    then group/average it by origin and dest at level 2.
    """
    if not year in ['2005','2010']:   raise ValueError('invalid year')         
    # read
    PATH_IMP_RAIL =  path.format(ROOT,year,tra_type) 
    df_rail = pd.read_csv(PATH_IMP_RAIL)
    # get _EZ2006_2 from _EZ2006_3 (truncate 7 first digits out of 9)
    df_rail['ORIGINZONE_2_ID']=df_rail['ORIGINZONE_3_ID'].apply(lambda x:str(x)[0:7])
    df_rail['DEST_ZONE_2_ID']=df_rail['DEST_ZONE_3_ID'].apply(lambda x:str(x)[0:7])
    # drop
    df_rail = df_rail.drop(['DEST_ZONE_3_ID','ORIGINZONE_3_ID'],axis=1) 
    # group at _EZ2006_2  
    grouped = df_rail.groupby(['ORIGINZONE_2_ID','DEST_ZONE_2_ID' ]) # BOTH!
    # average
    grouped_mean = grouped.mean().reset_index()
    grouped_mean=  grouped_mean.astype({'ORIGINZONE_2_ID':'int64','DEST_ZONE_2_ID':'int64'})
    return grouped_mean
 
def test_compare_impedance_eccentricity():
    # read ecc
    tra_type="rail"
    fname = 'data/impedance_eccentricity_rail_2010'
    ecc = np.load(fname+'.npy')
    # read graph
    d = load_etisplus(year='2010',geodata=True, data_src='observed', directed=False)
    g = d[tra_type]
    # remove node
    seq = g.vs.select(name_eq=1230000) 
    if len(seq)>0:
        idx = seq[0].index
        g.delete_vertices(idx)
    # get A    
    A = g.get_adjacency(type=GET_ADJACENCY_BOTH, attribute=None, default=0, eids=False)
    A = np.array(A.data)
    if np.any(A>1): 
        warnings.warn('multiple edge')
        A=np.clip(A,0,1)
    # compute some graph metrics 
    k=np.array(g.degree())
    knn_= knn_avg_UBCM(A,k)    
    # plot
    plt.hist(ecc,50); plt.show()
    plt.scatter(ecc,k); plt.show()
    plt.scatter(ecc,knn_); plt.show() 
    plt.scatter(k,knn_,c=ecc); 
    plt.colorbar();plt.show()

def test_impedance_eccentricity():
    # get node names
    year="2010"
    d = load_etisplus(year=year,geodata=True, data_src='observed', directed=False)
    # compute and save ecc
    fname = 'data/impedance_eccentricity_rail_2010.npy'
    impedance_eccentricity(d,fname,year="2010",tra_type="rail")
 
def impedance_eccentricity(d,fname,year="2010",tra_type="rail"):
    """
    see [Cazabet et al.17] 
    @inproceedings{cazabet2017enhancing, title={Enhancing Space-Aware Community Detection Using Degree Constrained Spatial Null Model}, author={Cazabet, Remy and Borgnat, Pierre and Jensen, Pablo}, booktitle={Workshop on Complex Networks CompleNet}, pages={47--55}, year={2017}, organization={Springer, Cham} }
    """
    # get grouped impedance
    imp=read_group_impedance_EZ2006_2()
    # match name and imp
    name = d[tra_type].vs['name']
    name.remove(1230000) # is not in name for some reason
    # get impedance
    mat = get_impedance_matrix(name,imp) 
    # compute ecc
    ecc = np.mean(mat,axis=0)
    # save
    np.save(fname,ecc) 
 
def test_get_impedance_matrix():
    """
    """
    # get grouped impedance
    imp=read_group_impedance_EZ2006_2()
    # get node names
    d = load_etisplus(year='2010',geodata=True, data_src='observed', directed=False)
    tra_type='rail'
    # match name and imp
    name = d[tra_type].vs['name']
    name.remove(1230000) # is not in name for some reason
    #name=pd.DataFrame({'ORIGINZONE_2_ID':d[tra_type].vs['name']})
    #m= pd.merge( df_imp.index.to_frame().reset_index(drop=True), name, how='inner')
    # get impedance
    mat = get_impedance_matrix(name,imp)  
    assert np.any(np.isnan(mat))
    """
    plt.matshow(mat);plt.show()
    """
    
def get_impedance_matrix(name,df_imp):
    """
    get impedance matrix for selected names only.
    """
    if not isinstance(name,list): raise ValueError('name must be a list')
    # convert to str 
    #if isinstance(name[0],int): name=[str(i) for i in name]
    # pivot all
    df_imp = df_imp.pivot(index='ORIGINZONE_2_ID', columns='DEST_ZONE_2_ID', values='p_imp_rail_time')
    #if not df_imp.index.is_monotonic : raise ValueError('index must be sorted')
    #if not df_imp.columns.is_monotonic : raise ValueError('columns must be sorted')
    # select
    mat = df_imp.loc[name][name].to_numpy()
    # postprocess
    np.fill_diagonal(mat,0)
    mat = np.nan_to_num(mat, copy=True, nan=0.0)
    return mat
    
# --------------------------------
# CONTROL: CLOSENESS
    
def test_compare_closeness_impedance():    
    # read
    # make net
    #  g_rail=Graph.TupleList(edge_set_rail, directed=directed, weights=True)
    # compute closeness
    # read etis 
    # compare to knn
    pass
# --------------------------------     
# CONTROL: SPATIAL ECCENTRICITY

def test_spatial_eccentricity():
    # compute
    year='2010'
    tra_type="rail"
    d = load_etisplus(year='2010',geodata=True, data_src='observed', directed=False)
    fname = 'data/spatial_eccentricity_{}_{}'.format(tra_type,year)
    spatial_eccentricity(d,fname,year="2010",tra_type=tra_type)
    # test load
    ecc = np.load(fname+'.npy')
    # plot
    plt.hist(ecc,100);plt.show() 
    
def test_compare_spatial_eccentricity():
    # read ecc
    fname = 'data/spatial_eccentricity_rail_2010'
    ecc = np.load(fname+'.npy')
    # read graph
    d = load_etisplus(year='2010',geodata=True, data_src='observed', directed=False)
    g = d[tra_type]
    # get A    
    A = g.get_adjacency(type=GET_ADJACENCY_BOTH, attribute=None, default=0, eids=False)
    A = np.array(A.data)
    if np.any(A>1): 
        warnings.warn('multiple edge')
        A=np.clip(A,0,1)
    # compute some graph metrics 
    k=np.array(g.degree())
    knn_= knn_avg_UBCM(A,k)
    # plot
    plt.scatter(ecc,k); plt.show()
    plt.scatter(ecc,knn_); plt.show()
    plt.scatter(k,knn_,c=ecc); 
    plt.colorbar();plt.show()
    
def spatial_eccentricity(d,fname,year="2010",tra_type="train"):
    """
    see [Cazabet et al.17] 
    @inproceedings{cazabet2017enhancing, title={Enhancing Space-Aware Community Detection Using Degree Constrained Spatial Null Model}, author={Cazabet, Remy and Borgnat, Pierre and Jensen, Pablo}, booktitle={Workshop on Complex Networks CompleNet}, pages={47--55}, year={2017}, organization={Springer, Cham} }
    """
    g = d[tra_type] 
    df = pd.DataFrame( {'ID': g.vs['name']})
    # join geo/rail
    df_merge = pd.merge(df,d['geodata'] )
    #df_EU = df_merge[['Name','geometry']]
    df_EU = df_merge[['ID','CENTROID']]
    # compute inter-distance
    D = get_distance_matrix(d,tra_type=tra_type)
    # compute spatial ecc
    ecc = np.mean(D,axis=0)
    # save
    np.save(fname,ecc)
    
# --------------------------------
# CONTROL: LAND FRACTION     
        
def test_load_land_polygon():
    """ """
    m = load_land_polygon(tol=0.1)
    # plot   
    bbox=[27,-33,70,38] 
    lat1,long1,lat2,long2=bbox    
    
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    for polygon in m:
        #plot_coords(ax, polygon.exterior)
        patch = PolygonPatch(polygon,fc='blue', ec='blue',alpha=0.5 )
        ax.add_patch(patch)
    ax.add_patch(patch)  
    ax.set_xlim(long1,long2 )
    ax.set_ylim(lat1, lat2)
    plt.show()

def load_land_polygon(path='land-polygons-complete-4326/land_polygons.shp',tol=0.05,
                    bbox=[27,-33,70,38]):    
    """ TOO SLOW!!!
        bbox: europe 
      
        Refs:
            bbox https://fiona.readthedocs.io/en/stable/manual.html#slicing-and-masking-iterators
        Data:
            https://osmdata.openstreetmap.de/data/water-polygons.html 
    """
    c_loc = "{}/{}".format(ROOT,path)
    #c_poly = [shape(pol['geometry']) for pol in fiona.open(c_loc)]
    # NB: fiona is long-lat
    lat1,long1,lat2,long2=bbox
    c = fiona.open(c_loc)
    hits = list(c.items(bbox=(long1, lat1,  long2, lat2)))
    # convert to shapely
    #c_poly = [shape(h[1]['geometry']) for h in hits]
    c_poly_simple = [shape(h[1]['geometry']).simplify(tol,preserve_topology=False) for h in hits]
    m=MultiPolygon(c_poly_simple)
    m_uni = unary_union(m)
    assert m_uni.is_valid
    return m_uni
    
def test_land_fraction_ratio():
    """ """
    m = load_land_polygon()
    p = Point(2.1015, 41.22) # BCN
    #p = Point(2.50215, 48.6808).buffer(1.5) # d['geodata'][d['geodata']['Name']=='Ile de France']['CENTROID']
    r = land_fraction_ratio(m, p, buf_size=1.5)
    
    # plot        
    fig = plt.figure(1)
    ax = fig.add_subplot(111)
    
    for polygon in m:
        #plot_coords(ax, polygon.exterior)
        patch = PolygonPatch(polygon,fc='blue', ec='blue',alpha=0.5 ) #   plt.scatter(df_x['5'],k); plt.show()
        ax.add_patch(patch)

    #patch = PolygonPatch(inter,fc='red', ec='red',alpha=0.5 )
    a = p.buffer(1.5) 
    patch = PolygonPatch(a,fc='red', ec='red',alpha=0.5 )
    ax.add_patch(patch)
    ax.set_xlim(long1,long2 )
    ax.set_ylim(lat1, lat2)
    plt.show()
    
def land_fraction_ratio(m, p, buf_size=1.5):    
    """ 
    compute the fraction of land in the neighborhood of point p
    
    Refs:
        https://shapely.readthedocs.io/en/latest/manual.html#object.difference
    """
    if not isinstance(m, shapely.geometry.multipolygon.MultiPolygon):
        raise ValueError('m must be MultiPolygon')
    if not isinstance(p, shapely.geometry.point.Point):
        raise ValueError('p must be Point')
    assert m.is_valid
    # compute intersection 
    a = p.buffer(buf_size) 
    inter = a.intersection(m)
    # area ratio
    return inter.area/a.area


def test_region_land_fraction_ratio():
    """ """
    path='data/region_land_fraction_ratio.npy'
    x = np.load(path)
    df = pd.DataFrame(x,columns=['ID','1.5','3','5'])
    df = df_x.astype({'ID': 'int64'})
    # 
    d = load_etisplus(year='2010',geodata=True, data_src='observed', directed=False)
    g = d['rail']
    A = g.get_adjacency(type=GET_ADJACENCY_BOTH, attribute=None, default=0, eids=False)
    A = np.array(A.data)
    if np.any(A>1): 
        warnings.warn('multiple edge')
        A=np.clip(A,0,1)
    k=np.array(g.degree())
    knn_= knn_avg_UBCM(A,k)
    # 
    plt.scatter(df_x['5'],k); plt.show()
    plt.scatter(df_x['5'],knn_); plt.show()

def region_land_fraction_ratio(buf_size=[1.5, 3, 5],path='data/region_land_fraction_ratio.npy'):
    """ """   
    d = load_etisplus(year='2010',geodata=True, data_src='observed', directed=False)
    g = d['rail'] 
    df = pd.DataFrame( {'ID': g.vs['name']})
    # join geo/rail
    df_merge = pd.merge(df,d['geodata'] )
    #df_EU = df_merge[['Name','geometry']]
    df_EU = df_merge[['ID','CENTROID']]
    for buf in buf_size:
        print(buf)
        df_EU[str(buf)] = df_EU['CENTROID'].apply( lambda p: land_fraction_ratio(m, p, buf_size=buf))     
    df_EU = df_EU.drop('CENTROID',axis=1)     
    # save    
    np.save(path,df_EU.to_numpy())



    
