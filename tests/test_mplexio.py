# -*- coding: utf-8 -*-
"""
Test file for the module mplex
Author:
    Aurélien Hazan
Description:
    This file contains the test cases for the functions and methods
    defined in mplexio.py. The tests can be run with ``pytest``.
Usage:
    To run the tests, execute
        $ pytest test_mplexio.py
    in the command line. If you want to run the tests in verbose mode, use
        $ pytest test_mplexio.py -v
    or
        $ pytest test_mplexio.py -v  -r P
    to capture the output of the SciPy solver.

    To run selected tests only, using regexp: 
        $pytest test_mplexio.py -k _igraph -v
"""
################################################################################
# Tests
################################################################################

import numpy as np
import matplotlib.pyplot as plt
from pymnet import *
from mplex.mplexio import *
from igraph import Graph

ROOT_DIR = '/home/aurelien/local/data/comtrade'


def test_read_ots_data_pymnet():
    path = '{}/yrpc-2000.csv'.format(ROOT_DIR)
    mplex = read_ots_data_pymnet(path)
    assert True

def test_read_ots_yrpc_data_igraph():
    path = '{}/yrpc-2000.csv'.format(ROOT_DIR)
    mplex = read_ots_yrpc_data_igraph(path)
    assert True

def test_read_ots_yrp_data_igraph():
    path = '{}/yrp-2000.csv'.format(ROOT_DIR)
    g = read_ots_yrp_data_igraph(path)
    assert isinstance(g,Graph)

def test_weight_read_ots_yrp_data_igraph():
    path = '{}/yrp-2000.csv'.format(ROOT_DIR)
    g = read_ots_yrp_data_igraph(path,directed=True)
    assert g.is_weighted()

def test_weight_directed_read_ots_yrpc_data_igraph():
    path = '{}/yrpc-2000.csv'.format(ROOT_DIR)
    mplex = read_ots_yrpc_data_igraph(path,directed=True)
    for layer,g in mplex.items() :
        assert g.is_weighted()
        assert g.is_directed()

def test_weight_undirected_read_ots_yrpc_data_igraph():
    path = '{}/yrpc-2000.csv'.format(ROOT_DIR)
    mplex = read_ots_yrpc_data_igraph(path,directed=False)
    for layer,g in mplex.items() :
        assert g.is_weighted()
        assert not g.is_directed()
