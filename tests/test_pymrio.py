# -*- coding: utf-8
"""
python3 -m pipenv shell
python

https://pymrio.readthedocs.io/en/latest/notebooks/working_with_exiobase.html#Getting-EXIOBASE

"""
import pymrio
import matplotlib.pyplot as plt
#------------------------------------------------------
# EXIOBASE

exio3 = pymrio.parse_exiobase3(path='/home/aurelien/local/data/MRIO/exiobase_3.4_iot_2011_ixi.zip')


exio3.meta
exio3.get_sectors()
exio3.get_regions()
exio3.calc_all()
list(exio3.get_extensions())
exio3.save_all('/home/aurelien/local/data/MRIO/exiobase_3.4_iot_2011_ixi.current')


import matplotlib.pyplot as plt

plt.figure(figsize=(15,15))
plt.imshow(exio3.A, vmax=1E-3)
plt.xlabel('Countries - sectors')
plt.ylabel('Countries - sectors')
plt.show()

with plt.style.context('ggplot'):
    exio3.impact.plot_account(['global warming (GWP100)'], figsize=(15,10))
    plt.show()

#------------------------------------------------------
# EORA
#from: https://pymrio.readthedocs.io/en/latest/notebooks/working_with_eora26.html
eora_storage="../../data/MRIO/eora26"
eora = pymrio.parse_eora26(year=2012, path=eora_storage)

import country_converter as coco

eora.aggregate(region_agg = coco.agg_conc(original_countries='Eora',
                                          aggregates=['OECD'],
                                          missing_countries='NON_OECD')
              )

eora.get_regions()
eora.calc_all()

with plt.style.context('ggplot'):
    eora.Q.plot_account(('Total cropland area', 'Total'), figsize=(8,5))
    plt.show()

# notebook: https://pymrio.readthedocs.io/en/latest/notebooks/buildflowmatrix.html
# notations: 
#        eora: https://worldmrio.com/eora26/
#          	transactions matrix T,
#	        primary inputs (also called value added) VA,
#           final demand block FD, 
#           satellite accounts  Q (Q for emissions associated with production, 
#                                 QY for direct emissions by final consumers)
#           (also called environmental extensions or stressors) 
#      
#        pymrio:  https://pymrio.readthedocs.io/en/latest/terminology.html
#                    
# 
# mathematical model:
list(eora.get_extensions())
?eora.Q
eora.Q.F #pandas.DataFrame,     Total direct impacts with columns as Z
eora.Q.F.head(5)
eora.Q.F[('OECD','Agriculture') ]['Energy Usage']
eora.Q.F[('OECD','Agriculture') ]['Energy Usage','Coal']

eora.Q.D_cba #Footprint of consumption


# TODO: tests
# /pymrio/tests/test_math.py : td_IO_Data_Miller , td_small_MRIO
# /pymrio/tests/mock_mrios/eora26_mock :  test ??  
