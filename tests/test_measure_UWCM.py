# -*- coding: utf-8 -*-
"""
Test file for the module mplex
Author:
    Aurélien Hazan
Description:
    This file contains the test cases for the functions and methods
    defined in measure.py. The tests can be run with ``pytest``.
Usage:
    To run the tests, execute
        $ pytest test_measure_UWCM.py
    in the command line. If you want to run the tests in verbose mode, use
        $ pytest test_measure_UWCM.py -v
    or
        $ pytest test_measure_UWCM.py -v  -r P
    to capture the output of the SciPy solver.

    To run selected tests only, using regexp: 
        $pytest test_measure_UWCM.py -k _igraph -v

"""

################################################################################
# Tests
################################################################################

import numpy as np
import matplotlib.pyplot as plt
from pymnet import *
from mplex.measure import *
from mplex.comtrade_multiplex import get_mplex_comtrade
from mplex.mplexio import *

from cm_ml.dataset import *


#TODO: remove that
ROOT_DIR = '/home/aurelien/local/data/comtrade'
  
####################################################
# SEVERAL BACKENDS

# SIMPLE
def test_link_overlap_simple_UW():
    """  UB: undirected weighted   """
    g1,g2 = get_simple_graphs(directed=False)
    mplex = {'a':g1,'b':g2 }
    layers = get_layers(mplex)
    lo_bin = link_overlap(mplex,layers,binary=False)
    assert lo_bin[('a','b')]==1.


def test_link_overlap_normalize_simple_UW():
    """     """
    g1,g2 = get_simple_graphs(directed=False)
    mplex = {'a':g1,'b':g2 }
    layers = get_layers(mplex)
    lo_bin = link_overlap(mplex,layers,binary=False)
    stren = multiplex_strength(mplex)
    lo_nor = link_overlap_normalize(lo_bin, stren)
    assert lo_nor[('a','b')]==2./10.


def test_link_overlap_average_simple_UW():
    """ ????????????? NOT TESTED YET ???????????????? """
    g1,g2 = get_simple_graphs(directed=False)
    mplex = {'a':g1,'b':g2 }
    d_pipj = fit_UWCM(mplex)
    layers = get_layers(mplex)
    lo_wei = link_overlap(mplex,layers,binary=False)
    stren = multiplex_strength(mplex)
    lo_nor = link_overlap_normalize(lo_wei, stren)
    lo_avg = link_overlap_average(stren, d_pipj, model='UWCM')
    assert True  
  
  
####################################################
# IGRAPH

# SIMPLE

def test_strength():
    """ """
    g1,g2 = get_simple_graphs(directed=False)
    assert g1.strength(weights='weight')==[3.,1.,2.]
    
def test_multiplex_strength_undir():
    """ """
    g1,g2 = get_simple_graphs(directed=False)
    d = {'a':g1,'b':g2 }
    l = multiplex_strength(d)
    assert l['a']==3. and l['b']==7.

def test_multiplex_strength_dir():
    """ """
    g1,g2 = get_simple_graphs(directed=True)
    d = {'a':g1,'b':g2 }
    l = multiplex_strength(d)
    assert l['a']==3. and l['b']==7.

def test_link_overlap_pairwise_igraph_undir_weighted():
    """   """
    g1,g2 = get_simple_graphs(directed=False)
    r=link_overlap_pairwise_igraph(g1,g2,binary=False)
    assert r==1.

def test_link_overlap_pairwise_igraph_dir_weighted():
    """  """
    g1,g2 = get_simple_graphs(directed=True)
    r=link_overlap_pairwise_igraph(g1,g2,binary=False)
    assert r==0

# DATA = AIRPORT, etc...

def test_fit_UWCM_airport_igraph():
    """ no normalization necessary with this dataset    """
    g1 = Graph.Read_Pajek('../cm_ml/data/USAir97.net')
    g2 = g1.copy()
    mplex = {'a':g1,'b':g2 }
    d_pipj = fit_UWCM_igraph(mplex)
    assert True

# DATA = COMTRADE

def test_solve_eq_UWCM_igraph():
    """ test low-level solve_eq_UWCM() rather than fit_UWCM_igraph() """
    path = '{}/yrp-2000.csv'.format(ROOT_DIR)
    g = read_ots_yrp_data_igraph(path)
    s = np.array(g.strength(weights='weight'))
    # normalization is necessary, else fails to converge
    s_norm = s/s.sum() 
    # solve for Pij under UWCM model    
    sol = solve_eq_UWCM(s_norm)
    assert sol.success
    P = pij_UWCM(sol.x)
    assert np.all( P<1) and np.all( P>=0)


def test_fit_UWCM_comtrade_igraph():
    """ normalization of mplex' weights is necessary   """
    mplex_com_undir =get_mplex_comtrade(year="2000", agg=True,forceUndirected=True,backend="igraph")
    mplex_cp = copy_mplex_igraph(mplex_com_undir)
    normalize_weight_mplex_igraph(mplex_cp,alg="total")
    d_pipj = fit_UWCM_igraph(mplex_cp)
    assert True

# ----------
# OVERLAP

def test_link_overlap_rescale_comtrade_agg_igraph_UWCM():
    """ normalization of mplex' weights is necessary   """
    mplex_com_undir =get_mplex_comtrade(year="2000", agg=True,forceUndirected=True,backend="igraph")
    mplex_cp = copy_mplex_igraph(mplex_com_undir)
    normalize_weight_mplex_igraph(mplex_cp,alg="total")
    d_pipj = fit_UWCM_igraph(mplex_cp)
    #    
    layers = get_layers(mplex_cp)
    lo_wei = link_overlap(mplex_cp,layers,binary=False)
    stren = multiplex_strength(mplex_cp)
    lo_nor = link_overlap_normalize(lo_wei, stren)
    lo_avg = link_overlap_average(stren, d_pipj, model='UWCM') 
    lo_resc = link_overlap_rescale(lo_nor, lo_avg, layers= lo_avg.keys(), 
                                    binary=False)  
    assert True


def test_link_overlap_rescaled_igraph():
    """ compare 2 implementations"""
    mplex_com_undir =get_mplex_comtrade(year="2000", agg=True,forceUndirected=True,backend="igraph")
    mplex_cp = copy_mplex_igraph(mplex_com_undir)
    normalize_weight_mplex_igraph(mplex_cp,alg="factor",K=1000000000.) # alg = ?????
    d_pipj = fit_UWCM_igraph(mplex_cp)
    # 
    lo_resc = link_overlap_rescaled_igraph(mplex_cp,d_pipj)
    # comparison
    layers = get_layers(mplex_cp)
    lo_wei = link_overlap(mplex_cp,layers,binary=False)
    stren = multiplex_strength(mplex_cp)
    lo_nor = link_overlap_normalize(lo_wei, stren)

    assert True
    """
    plot_overlap_mat(lo_nor,lo_resc)
    #https://matplotlib.org/gallery/statistics/histogram_multihist.html#sphx-glr-gallery-statistics-histogram-multihist-py
    colors=['b','r']; labels=['nor','resc']
    n_bins=50
    plt.hist( [list(lo_nor.values()), list(lo_resc.values())], n_bins,
            histtype='bar', color=colors, label=labels)
    plt.title('UWCM. compare to [Gemmetto18] Fig.1.1 ')
    plt.legend()
    plt.show()
    """




def test_link_overlap_zscore_comtrade_UWCM():
    pass
