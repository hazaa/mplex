# -*- coding: utf-8 -*-
"""
adapted from ExampleUsageSpatialNullM.py
in github.com/yquetzal/spaceCorrectedLouvainDC

REF:
@inproceedings{cazabet2017enhancing,
 title={Enhancing Space-Aware Community Detection Using Degree Constrained Spatial Null Model},
 author={Cazabet, Remy and Borgnat, Pierre and Jensen, Pablo}, 
 booktitle={Workshop on Complex Networks CompleNet},
 pages={47--55}, year={2017}, organization={Springer, Cham} }
"""
from cfg import *
from spaceCorrectedLouvainDC.Tools.spatialNullModel import *
from spaceCorrectedLouvainDC.Tools.utilSpatialNullM import *
import matplotlib.pyplot as plt
import networkx as nx
import pandas as pd
import numpy as np
###-------------- LOAD GRAPH, DISTANCES
ROOT_spaceCorrectedLouvainDC = "{}/../git/extern/spaceCorrectedLouvainDC".format(ROOT)
plot=False
#exemples of files to handle
flowFile = "{}/files/Global2011.txt".format(ROOT_spaceCorrectedLouvainDC)
distanceFile = "{}/files/distancesLyon.txt".format(ROOT_spaceCorrectedLouvainDC)

#obtain the observed network as a networkx object
observedNetwork = readVelovFileAsNetworkX(flowFile)
#create an undirected version of original graph by summing the weight of edges in both directions, for community detection
undirectedObservedGraph = createUndirectedGraphWithSumWeight(observedNetwork)

#obtain the distance between nodes as a function that takes 2 nodes and return their distance
# In little example like this one, precomputed because faster, but could be computed on the fly
distancesBetweenNodes = Distances()
distancesBetweenNodes.getDistanceFunctionVelov(distanceFile)

# observedNetwork is DENSE
w=np.array([t[2]['weight'] for t in observedNetwork.edges(data=True)] )
print( np.sum(1*(w==0)) )
print(np.sum(1*(w==1))/w.shape[0])
# but exponential distribution of weigths
plt.hist(w,bins=100,log=True);plt.show()

# Spatial eccentricity
df=pd.DataFrame(distancesBetweenNodes.allDistances.items(),columns=['OD','dist'])
df['origin']=df['OD'].apply(lambda x:x[0])
df['destination']=df['OD'].apply(lambda x:x[1])
df = df.drop(columns=['OD'])
ecc_out = df.groupby(['origin']).mean().reset_index()
ecc_in = df.groupby(['destination']).mean().reset_index()

k_in = pd.DataFrame(observedNetwork.in_degree(),columns=['station','k_in']) 
k_out = pd.DataFrame(observedNetwork.out_degree(),columns=['station','k_out']) 

merge = pd.merge(ecc_out.rename(columns={'origin':"station"}), k_in, how='inner')
plt.scatter(merge['k_in'],merge['dist']); plt.xlabel('k_in'); plt.ylabel('ecc');plt.show()

merge = pd.merge(ecc_in.rename(columns={'destination':"station"}), k_out, how='inner')
plt.scatter(merge['k_out'],merge['dist']); plt.xlabel('k_out'); plt.ylabel('ecc');plt.show()

###-------------- COMPUTE NULL MODEL

# DEGREE CORRECTED spatial network. roundDecimal = -2 : bin every 100 unity (meters)
(nullModel, distFunction) = getSpatialNullModel(observedNetwork,distancesBetweenNodes.getDistanceBetween,roundDecimal=-2,iterations=5,plot=plot)
#compute undirected version of null model
graphOfNullModel = createnxGraphFromGraphModel(nullModel)

# knn
#A = nx.adjacency_matrix(graphOfNullModel)
knn_in_m = nx.average_neighbor_degree(graphOfNullModel, source='in', target='in',weight='weight')
knn_out_m = nx.average_neighbor_degree(graphOfNullModel, source='out', target='out',weight='weight')
k_in_m,k_out_m = nullModel.getDegrees()  # != G.in_degree(), G.out_degree(), G = nx.DiGraph(graphOfNullModel)
# plot
x=np.array([ (k, knn_in_m[station]) for station,k in k_in_m.items()] )
plt.scatter(x[:,0],x[:,1],color="r")
#
knn_in = nx.average_neighbor_degree(observedNetwork, source='in', target='in',weight='weight')
knn_out = nx.average_neighbor_degree(observedNetwork, source='out', target='out',weight='weight')
k_in = observedNetwork.in_degree(weight='weight')
k_out = observedNetwork.out_degree(weight='weight')
# plot
x=np.array([ (k, knn_in[station]) for station,k in dict(k_in).items()] )
plt.scatter(x[:,0],x[:,1],color="b")
plt.xlabel('k_in'); plt.ylabel('k^nn')

plt.show()

# spatial eccentricity ? =fixed spatial property, doesn't depend on the model
# TODO: scholar + ecc ; barth + ecc ; spatial model + ecc

#SIMPLE CONFIGURATION MODEL (ingore spatial effect)
nullModel = ConfigurationModel(dict(observedNetwork.in_degree(weight="weight")),dict(observedNetwork.out_degree(weight="weight")))
graphOfNullModel = createnxGraphFromGraphModel(nullModel)



