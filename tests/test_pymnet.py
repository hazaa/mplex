# -*- coding: utf-8 -*-
"""
http://www.mkivela.com/pymnet/

NB: following line must be inserted in pymnet.diagnostics.py 
#from pymnet.transforms import subnet,threshold,aggregate #AH
"""
import numpy as np
from pymnet import *
ROOT_DIR = '/home/aurelien/local/data'


# multilayer
mnet = MultilayerNetwork(aspects=1)
# add an edge between node 1 in layer "a" to node 2 in layer "b" (again, the nodes and layers are implicitely created)
mnet[1,2,'a','b'] = 1

# multiplex
mplex = MultiplexNetwork(couplings="none")
mplex[1,'a'][2,'a']=1
mplex.A['a'][1,2]

# basic properties
mplex[1,'a'].deg()
mplex['a'].deg()

# get nodes/layers
nodes = mplex.slices[0]
set(mplex.A['a'])
layers= mplex.slices[1] 
mplex['a']


# nx function
mplex = er(10000,3*[0.01])

# indicators
# see tests/diagnostics_tests.py:test_multiplex_*
print( map(nx.number_connected_components,mplex.A.values()) )
print( cc.avg_lcc_aw(mplex) )
print( diagnostics.multiplex_density(mplex) )
print( diagnostics.multiplex_degs(mplex,degstype="distribution") )
print( diagnostics.overlap_degs(mplex) )
# http://www.mkivela.com/pymnet/reference.html#module-pymnet.cc
# cc_barrett, lcc_brodka, gcc_aw, sncc_aw, elementary_cycles

# get neighbors
[neigh for neigh in mplex.A['a'][1]]

# ----------
# plot
# http://www.mkivela.com/pymnet/visualizing.html
fig=draw(mplex ,layout="spring", show=True)
# nxwrap
nodes = mplex.slices[0]
sub_net = subnet(mplex, nodes, 'a')
nnx=nxwrap.MonoplexGraphNetworkxView(sub_net)
nxwrap.draw(nnx) #FAILS
plt.show()
# single linkage ?
# ----------
# IO
# http://vlado.fmf.uni-lj.si/pub/networks/data/ucinet/ucidata.htm#bkfrat
path = '{}/ucinet/bkfrat.dat'.format(ROOT_DIR)
net=read_ucinet(path,couplings="none")
net=transforms.threshold(net,4)
fig=draw(net,
             show=True,
             layout="spring",
             layerColorRule={},
             defaultLayerColor="gray",
             nodeLabelRule={},
             edgeColorRule={"rule":"edgeweight","colormap":"jet","scaleby":0.1}
             )
             
# other examples, akin to karate club: https://qiita.com/malimo1024/items/499a4ebddd14d29fd320       
             
# exos: sayama; georgiev tutorial ; zinoviev (panama)

# --------------------------------
# graphlets ?? not multiplex
# https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5103263/
#pymnet.graphlets.GCD
# interlayer dependencies: spearman
#pymnet.graphlets.graphlet_measures.spearmanr
