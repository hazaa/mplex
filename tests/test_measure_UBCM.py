# -*- coding: utf-8 -*-
"""
Test file for the module mplex
Author:
    Aurélien Hazan
Description:
    This file contains the test cases for the functions and methods
    defined in measure.py. The tests can be run with ``pytest``.
Usage:
    To run the tests, execute
        $ pytest test_measure_UBCM.py
    in the command line. If you want to run the tests in verbose mode, use
        $ pytest test_measure_UBCM.py -v
    or
        $ pytest test_measure_UBCM.py -v  -r P
    to capture the output of the SciPy solver.

    To run selected tests only, using regexp: 
        $pytest test_measure_UBCM.py -k _igraph -v

"""

################################################################################
# Tests
################################################################################

import numpy as np
import matplotlib.pyplot as plt
from pymnet import *
from mplex.measure import *
from mplex.comtrade_multiplex import get_mplex_comtrade
from mplex.mplexio import *

from cm_ml.dataset import *


#TODO: remove that
ROOT_DIR = '/home/aurelien/local/data/comtrade'
  

mplex_com = get_mplex_comtrade(year="2000", backend="pymnet")
#mplex_com_undir =get_mplex_comtrade(year="2000", agg=True,forceUndirected=True,backend="pymnet")
#mplex_com_undir =get_mplex_comtrade(year="2000", agg=True,forceUndirected=True,backend="igraph")
mplex_com_undir_full =get_mplex_comtrade(year="2000", agg=False,forceUndirected=True,backend="igraph")
mplex_com_undir = dict(filter(lambda x: x[0][-1]=='0' ,[(k,v)  for (k,v) in mplex_com_undir_full.items()]))

d_pipj = fit_UBCM(mplex_com_undir)




####################################################
# PYMNET

def test_multiplex_nlink_pymnet():
    """     """
    mplex = mplex_com_undir
    if isinstance(mplex,MultiplexNetwork):
        if mplex.directed: raise ValueError('works only with undirected net')
            
        nlink = multiplex_nlink(mplex)
        layers = mplex.slices[1]
        for layer in layers:
            d= get_sorted_degree(mplex,layer)
            assert sum(d)/2 == nlink[layer]

def test_fit_UBCM():
    """     """
    mplex = mplex_com_undir
    d_pipj = fit_UBCM(mplex)
    assert True

####################################################
# IGRAPH

"""
THIS FAILS 
def test_link_overlap_pairwise_igraph_undir():
   
    edg1 = [('a','b',1.),('a','c',2.)  ]
    edg2 = [('b','a',3.),('b','c',4.)  ]
    g1 = Graph.TupleList( edg1, directed=False,edge_attrs="weight")
    g2 = Graph.TupleList( edg2, directed=False,edge_attrs="weight")
    r=link_overlap_pairwise_igraph(g1,g2,binary=True)
    assert r==1
"""
# SIMPLE


def test_link_overlap_pairwise_igraph_undir_bin():
    """     """
    g1,g2 = get_simple_graphs(directed=False)
    r=link_overlap_pairwise_igraph(g1,g2,binary=True)
    assert r==1

def test_link_overlap_pairwise_igraph_dir_bin():
    """     """
    g1,g2 = get_simple_graphs(directed=True)
    r=link_overlap_pairwise_igraph(g1,g2,binary=True)
    assert r==0


# DATA=AIRPORT etc..
def test_fit_UBCM_airport_igraph():
    """ no normalization necessary with this dataset    """
    pass


def test_Adjacency():
    """create a graph from an adjacency matrix  """
    g = Graph.Read_Pajek('../cm_ml/data/USAir97.net')     
    k = g.degree()
    k_ar=np.array( k ,dtype=np.int32 )
    sol = solve_eq_UBCM(k_ar) 
    assert sol.success
    P = pij_UBCM(sol.x)
    g_CM = Graph.Adjacency(P.tolist(), mode=ADJ_UNDIRECTED)
    assert

def test_compare_real_CM_igraph_UBCM(): 
    """ """ 
    g = Graph.Read_Pajek('../cm_ml/data/USAir97.net')
    mplex = {'a':g}
    d_pipj = fit_UBCM_igraph(mplex)
    r = compare_real_CM_igraph(mplex,d_pipj,model="UBCM",plot=True,threshold=None)
    assert True
   
   
# DATA = COMTRADE

def test_fit_UBCM_igraph():
    """     """
    mplex_com_dir =get_mplex_comtrade(year="2000", agg=True,forceUndirected=False,backend="igraph")
    d_pipj = fit_UBCM_igraph(mplex_com_dir)
    assert True

def test_multiplex_nlink_igraph():
    """     """
    mplex_com_dir =get_mplex_comtrade(year="2000", agg=True,forceUndirected=False,backend="igraph")
    d = multiplex_nlink(mplex_com_dir)
    assert True

def test_ratio_ns_nd():
    """  this one is a graph measure (not mplex)   """
    path = '{}/yrp-2000.csv'.format(ROOT_DIR)
    g = read_ots_yrp_data_igraph(path)
    ratio_ns_nd(g,mode='in')
####################################################
# SEVERAL BACKENDS

# SIMPLE

def test_link_overlap_simple_UB():
    """  UB: undirected binary   """
    g1,g2 = get_simple_graphs(directed=False)
    mplex = {'a':g1,'b':g2 }
    layers = get_layers(mplex)
    lo_bin = link_overlap(mplex,layers,binary=True)
    assert lo_bin[('a','b')]==1


def test_link_overlap_normalize_simple_UB():
    """     """
    g1,g2 = get_simple_graphs(directed=False)
    mplex = {'a':g1,'b':g2 }
    layers = get_layers(mplex)
    lo_bin = link_overlap(mplex,layers,binary=True)
    nlink = multiplex_nlink(mplex)
    lo_nor = link_overlap_normalize(lo_bin, nlink)
    assert lo_bin[('a','b')]==1./2.


def test_link_overlap_average_simple_UBCM():
    """  ????????????? NOT TESTED YET ????????????????   """
    g1,g2 = get_simple_graphs(directed=False)
    mplex = {'a':g1,'b':g2 }
    d_pipj = fit_UBCM(mplex)
    layers = get_layers(mplex)
    lo_glob_bin = link_overlap(mplex,layers,binary=True)
    nlink = multiplex_nlink(mplex)
    lo_nor = link_overlap_normalize(lo_glob_bin, nlink)
    lo_avg = link_overlap_average(nlink, d_pipj, model='UBCM')
    assert True


    
# ----------
# OVERLAP


def test_link_overlap_comtrade_UBCM():
    """     """
    mplex = mplex_com_undir
    layers = get_layers(mplex)
    lo_glob_bin = link_overlap(mplex,layers,binary=True)
    assert True

def test_link_overlap_normalize_comtrade_UBCM():
    """     """
    mplex = mplex_com_undir
    layers = get_layers(mplex)
    lo_glob_bin = link_overlap(mplex,layers,binary=True)
    nlink = multiplex_nlink(mplex)
    lo_nor = link_overlap_normalize(lo_glob_bin, nlink)
    assert True

def test_link_overlap_average_comtrade_UBCM():
    """     """
    mplex = mplex_com_undir
    #d_pipj = fit_UBCM(mplex)
    layers = get_layers(mplex)
    lo_glob_bin = link_overlap(mplex,layers,binary=True)
    nlink = multiplex_nlink(mplex)
    lo_nor = link_overlap_normalize(lo_glob_bin, nlink)
    lo_avg = link_overlap_average(nlink, d_pipj, model='UBCM')
    assert True

def test_link_overlap_rescale_comtrade_UBCM():
    """     """
    mplex = mplex_com_undir
    #d_pipj = fit_UBCM(mplex)
    layers = get_layers(mplex)
    lo_glob_bin = link_overlap(mplex,layers,binary=True)
    nlink = multiplex_nlink(mplex)
    lo_nor = link_overlap_normalize(lo_glob_bin, nlink)
    lo_avg = link_overlap_average(nlink, d_pipj, model='UBCM')
    # some layers might be missing in lo_avg
    lo_resc = link_overlap_rescale(lo_nor, lo_avg, layers= lo_avg.keys(), 
                                    binary=True)
    """
    plot_overlap_mat(lo_nor,lo_resc)
    #https://matplotlib.org/gallery/statistics/histogram_multihist.html#sphx-glr-gallery-statistics-histogram-multihist-py
    colors=['b','r']; labels=['nor','resc']
    n_bins=50
    plt.hist( [list(lo_nor.values()), list(lo_resc.values())], n_bins,
            histtype='bar', color=colors, label=labels)
    plt.title('UBCM. compare to [Gemmetto18] Fig.1.1 ')
    plt.legend()
    plt.show()
    """
    assert True

def test_link_overlap_std_comtrade_UBCM():
    """     """    
    mplex = mplex_com_undir
    #d_pipj = fit_UBCM(mplex)
    nlink = multiplex_nlink(mplex)
    lo_std = link_overlap_std(nlink,d_pipj, model='UBCM')
    assert True

def test_link_overlap_zscore_comtrade_UBCM():
    """     """
    mplex = mplex_com_undir
    #d_pipj = fit_UBCM(mplex)
    layers = get_layers(mplex)
    lo_glob_bin = link_overlap(mplex,layers,binary=True)
    nlink = multiplex_nlink(mplex)
    lo_nor = link_overlap_normalize(lo_glob_bin, nlink)
    lo_avg = link_overlap_average(nlink, d_pipj, model='UBCM')
    # some layers might be missing in lo_avg
    lo_resc = link_overlap_rescale(lo_nor, lo_avg, layers= lo_avg.keys(),
                                binary=True)
    lo_std = link_overlap_std(nlink,d_pipj, model='UBCM')
    lo_zscore_UBCM = link_overlap_zscore(lo_nor, lo_avg, lo_std,
                                        layers= lo_avg.keys())
    """
    p=np.array([ (lo_resc[k],lo_zscore_UBCM[k] )  for k in lo_resc.keys()])
    plt.scatter( p[:,0],p[:,1]  );
    plt.xlabel('resc'); plt.ylabel('zscore')
    plt.title('UBCM. Compare to [Gemmetto18] Fig. 1.7 ')
    plt.show()
    """
    assert True


