# -*- coding: utf-8 -*-

"""
Test file for the module mplex
Author:
    Aurélien Hazan
Description:
    This file contains the test cases for the functions and methods
    defined in tools.py. The tests can be run with ``pytest``.
Usage:
    To run the tests, execute
        $ pytest test_tools.py
    in the command line. If you want to run the tests in verbose mode, use
        $ pytest test_tools.py -v
    or
        $ pytest test_tools.py -v  -r P
    to capture the output of the SciPy solver.

    To run selected tests only, using regexp: 
        $pytest test_tools.py -k _igraph -v

"""

import numpy as np
#import matplotlib.pyplot as plt
from pymnet import *
from mplex.tools import *


def test_copy_mplex_igraph():
    """ """
    g1,g2 = get_simple_graphs(directed=False)    
    mplex = {'a':g1,'b':g2 }
    mplex_copy = copy_mplex_igraph(mplex)
    mplex['a'].es['weight'] = [10. , 20.]
    assert mplex_copy['a'].es['weight'] == [1. , 2.]


def test_normalize_weight_mplex_igraph():
    """     """
    g1,g2 = get_simple_graphs(directed=False)    
    mplex = {'a':g1,'b':g2 }
    normalize_weight_mplex_igraph(mplex,alg="total")
    assert  mplex['a'].es['weight'] == [1./10. , 2./10.]
    
    
