# -*- coding: utf-8 -*-
"""
Tools
"""
import numpy as np
try:
    from pymnet import *
except:
    print('pymnet could not be loaded')   
from igraph import *

try:
    import networkx  as nx  
except:
    print('nx could not be loaded')   
#######################################################
# FUNCTIONS WITH SUPPORT FOR SEVERAL BACKENDS 

def convert_ig_to_nx(g):
    """
    convert igraph Graph to networkx
    
    https://stackoverflow.com/questions/29783828/convert-python-igraph-graph-to-networkx
    """
    A = g.get_edgelist()
    if g.is_directed():        
        G = nx.DiGraph(A) # In case your graph is directed
    else: 
        G = nx.Graph(A)
    # in case some nodes don't appear in the edgeset
    nodes= [v.index for v in g.vs]
    G.update(nodes =nodes )            
    #weights
    if g.is_weighted():   
        for e in g.es:
            G[ e.source][e.target]['weight'] = e['weight']
    #names        
    for v in g.vs:
        G.nodes[v.index]['name'] = v['name']           
    return G

def multiplex_strength(net):
    """Returns a dictionary of the total link strength of each intra-layer
        network of a multiplex network.
    """
    d={}
    if isinstance(net,MultiplexNetwork):
        raise NotImplementedError
    elif isinstance(net, dict):
        for layer,g in net.items():
            d[layer]=sum(g.es['weight'] )
    return d 
    
def multiplex_nlink(net):
    """Returns a dictionary of the link number of each intra-layer network 
        of a multiplex network.
    """
    d={}
    if isinstance(net,MultiplexNetwork):
        for layer in net.iter_layers():		
            d[layer]= len(net.A[layer].edges)
    elif isinstance(net, dict):
        for layer,g in net.items():
            d[layer]=g.ecount()
    return d        
    

#######################################################
# IGRAPH FUNCTIONS

def get_simple_graphs(directed=False):
    """ test function    """
    g1=Graph(directed=directed)
    g1.add_vertices(['a','b','c'])
    g1.add_edges([('a','b'), ('a','c') ])
    g1.es['weight'] = [1. , 2.]
    #g1.get_edgelist() 
    g2=Graph(directed=directed)
    g2.add_vertices(['a','b','c'])
    g2.add_edges([('b','a'), ('b','c') ])
    g2.es['weight'] = [3. , 4.]
    #g2.get_edgelist() 
    return g1,g2

def copy_mplex_igraph(mplex):
    """
    deep copy the dictionary of graphs
    """
    mplex_copy = {}
    for k,g in mplex.items():
        mplex_copy[k]=g.copy()
    return mplex_copy    

def normalize_weight_mplex_igraph(mplex,alg="total",K=1.):
    """
    Normalize the weights of the multiplex
    
    Input:
    ------
    alg: str
    "total" normalize each weight wrt the total sum across layers
    """
    stren = multiplex_strength(mplex)
    if alg == "total":
        S = sum(stren.values())
    elif alg == "max":
        S = max(stren.values())
    elif alg == "factor":    
        S = K
    else: raise ValueError('unknown alg')    
    for k,g in mplex.items():
        g.es['weight'] = np.array(g.es['weight'])/S
        
