# -*- coding: utf-8 -*-
"""
DATA: ots, from comtrade
	https://comtrade.un.org/data/Doc/api/ex
	https://docs.tradestatistics.io/
TIME-DEPENDENCE: no

NETWORK TYPE: multiplex

DEPS: pymnet, igraph, py3plex, python-louvain, fa2

REFS:
    see measure.py
    [Barigozzi et al 09] "Multinetwork of international trade: a commodity-specific analysis" 
                        arxiv:0908.1879

LINKS:
https://networkx.github.io/documentation/stable
https://github.com/SkBlaz/Py3plex
https://igraph.org/python/#docs
http://cran.irsn.fr/web/packages/multinet/index.html
"""
import numpy as np
import matplotlib.pyplot as plt
#import heapq,itertools
from pymnet import *
#from pymnet.transforms import subnet,threshold,aggregate 

from igraph import *
#from functools import reduce

from mplex.mplexio import *
from mplex.measure import *

from fa2 import ForceAtlas2

#TODO: move ROOT_DIR somewhere
# https://docs.python.org/3/faq/programming.html?highlight=global#how-do-i-share-global-variables-across-modules
ROOT_DIR = '/home/aurelien/local/data/comtrade'

# -------------------------------------------
# BACKENDS : pymnet, multinetx, py3plex


def test_spatial_mplex():
    # TODO
    pass 
def test_time_dep():
    # TODO; cf Py3plex
    # TODO: link-stream compression https://github.com/Lamarche-Perrin/multidimensional_compression
    # TODO: https://github.com/Yquetzal/tnetwork
    pass    

def test_time_independent(year="2000",backend='pymnet'):    
    PATH = '{}/comtrade_ots_data_agg_{}.csv'.format(ROOT_DIR,year)
    path = PATH
    if backend=='pymnet':
        pipeline_pymnet(PATH)
    else: raise ValueError('unknown backend')    

# TODO: move to mplexio
def get_mplex_comtrade(year="2000", agg=True,forceUndirected=False, backend='pymnet'):
    """
    """
    if forceUndirected: directed = False
    else: directed = True
    if agg:
        # REPLACE THIS WITH: read_ots_yrpc_data_igraph(path,directed=True,imports=True,digit=1)        
        if backend=='pymnet':
            path = '{}/comtrade_ots_data_agg_{}.csv'.format(ROOT_DIR,year)	
            return read_mplex_edge_files(path,couplings='none',fullyInterconnected=True,directed=directed,ignoreSelfLink=True)
        elif backend=='igraph':
            path = '{}/yrpc-{}.csv'.format(ROOT_DIR,year)
            return read_ots_yrpc_data_igraph(path,directed=directed,imports=True,digit=1)        
            #return read_mplex_edge_file_igraph(path,directed=directed, sep=',')
        else: raise NotImplemented('unknown backend')
    else:
        path = '{}/yrpc-{}.csv'.format(ROOT_DIR,year)
        if backend=='pymnet':
            return read_ots_data_pymnet(path,directed=directed)
        elif backend=='igraph':
            return read_ots_yrpc_data_igraph(path,directed=directed)
        else: raise NotImplemented('unknown backend')
    

# -------------------------------------------
# ANALYZE MPLEX

def analyze_single_layer_igraph(g):
    """

    GraphBase methods: fast (coded in c)
       https://igraph.org/python/doc/igraph.GraphBase-class.html
    Graph: not speed critical (coded in py)              
       https://igraph.org/python/doc/igraph.Graph-class.html
    """
    r={}
    r['density']=g.density()
    #https://igraph.org/python/doc/igraph.GraphBase-class.html#assortativity_degree
    #betweenness  dir
    r['betweenness']=g.betweenness()
    #diameter(directed=True
    r['diameter']=g.diameter(directed=True)
    #(eigenvector_centrality(directed=True )
    # degree
    r['deg_in']=g.indegree()
    r['deg_out']=g.outdegree()
    #knn( not dir
    #reciprocity
    #strength
    r['str_in'] = g.strength(mode='in',weights='weight')
    r['str_out'] = g.strength(mode='out',weights='weight')
    #NSin/NDin
    r['NSin/NDin']= np.nan_to_num(ratio_ns_nd(g,mode='in')).mean()
    r['NSout/NDout']= np.nan_to_num(ratio_ns_nd(g,mode='out')).mean()
    #--------    
    # basic stats (Graph)
    # clustering https://igraph.org/python/doc/igraph.GraphBase-class.html#transitivity_local_undirected
    #evcent dir, eigenvector centralities 
    r['evcent']=g.evcent()
    #components(mode=STRONG)
    r['components']=g.components()
    #pagerank dir
    r['pagerank']=g.pagerank()
    #--------
    # intra-layer corr
    r['cor_intra_deg_str_in']=np.corrcoef(r['deg_in'], r['str_in'])
    r['cor_intra_deg_str_out']=np.corrcoef(r['deg_out'], r['str_out'])
    #--------    
    # community detection
    # monoplex
    #   community_infomap  py
    return res
    
def analyze_pymnet(mplex):
    """
    http://www.mkivela.com/pymnet/reference.html#module-pymnet.cc
    """
    res={}
    # --
    # clustering
    res['avg_lcc_aw']=cc.avg_lcc_aw(mplex) 
    #print( diagnostics.overlap_degs(mplex) ) # ?????
    # cc_barrett, anet??
    # cc.lcc_brodka(mplex,'afg')  # NODE-DEPENDENT
    res['gcc_aw']=cc.gcc_aw(mplex)
    #cc.sncc_aw(mplex,'alb')      # NODE-DEPENDENT
    #elementary_cycles(mplex)
    # --    
    # correlation: 
    #link_glob,link_loc = link_overlap(mplex,output='dict',normalize=True)
    #link_glob = link_overlap2(mplex,normalize='max')
    deg_cor = degree_correlation(mplex,output='dict')
    res['deg_cor']=deg_cor
    # --
    # rescale link overlap (à la Gemmetto)
    # UBCM    
    layers = mplex.slices[1]
    lo_glob_bin = link_overlap(mplex,layers,binary=True) 
    res['lo_glob_bin']=lo_glob_bin
    d_pipj = fit_UBCM(mplex)
    #    rescale
    nlink = multiplex_nlink(mplex)
    lo_nor = link_overlap_normalize(lo_glob_bin,nlink)
    lo_avg = link_overlap_average(nlink, d_pipj, model='UBCM')
    lo_resc = link_overlap_normalize(lo_nor, lo_avg, binary=True)
    res['lo_glob_bin_resc']= lo_resc
    #    z-score
    lo_std = link_overlap_std(d_pipj, model='UBCM')
    lo_zscore_UBCM = link_overlap_zscore(lo_nor, lo_avg, lo_std)
    res['lo_zscore_UBCM'] = lo_zscore_UBCM
    # --
    # UWCM
    lo_glob_wei = link_overlap(mplex,binary=False) 
    res['lo_glob_wei']=lo_glob_wei
    # --
    # DIRECTED UNWEIGHTED    
    # --
    # DIRECTED WEIGHTED
    return res 

def analyze_single_layer_nx(net,plot=False):
    """
    COMPARE WITH RANDOMIZED MODEL!! 
    """
    res={}
    # -------------------------------------------                    
    # basic properties
    net_undir= net.to_undirected()
    # p(k_in), p(k_out), 
    d = net.in_degree()
    res['in_degree']=d
    
    d = net.out_degree()
    res['out_degree']=d
    d=net.out_degree()
    # strength of nodes
    str_out = net.out_degree(weight='weight')
    res['out_degree'] = str_out
    str_in = net.in_degree(weight='weight')
    res['in_degree'] = str_in
    # inverse participation rate IPR
    # ?
    # clustering,
    clust = nx.clustering(net)
    res['clustering'] =clust
    # centrality
    # https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.centrality.betweenness_centrality.html#networkx.algorithms.centrality.betweenness_centrality
    # connected components
    #   directed/undirected, strong/weak, tube/tendrils (cf bianconi §2)
    #   https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.components.weakly_connected_components.html#networkx.algorithms.components.weakly_connected_components
    #comp = sorted(nx.weakly_connected_components(net),key=len, reverse=True)
    #len_weak_conn_comp = [len(c) for c in comp]
    res['component_strong']= sorted(nx.strongly_connected_components(net),key=len, reverse=True)
    # diameter vs log(N)
    """ TOO SLOW
    try:
        diam_dir = nx.diameter(net)
    except nx.NetworkXError:
        pass    
    try:    
        diam_undir = nx.diameter(net_undir)
    except nx.NetworkXError: 
        pass
    """    
    # eigenvalues ? https://networkx.github.io/documentation/stable/auto_examples/advanced/plot_eigenvalues.html#sphx-glr-auto-examples-advanced-plot-eigenvalues-py
    """ TOO SLOW
    L = nx.normalized_laplacian_matrix(net_undir)
    e = np.linalg.eigvals(L.A)
    if plot:
        plt.hist(e, bins=100) 
        plt.show()
    """    
    # small-world property https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.smallworld.sigma.html#networkx.algorithms.smallworld.sigma
    #nx.algorithms.smallworld.sigma(G_undir) # TOO SLOW!!
    #nx.algorithms.smallworld.omega(G_undir)# TOO SLOW!!
    # scale-free ?
    # -------------
    # assortativity:  average-degree-connectivity
	#https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.assortativity.average_degree_connectivity.html?highlight=nearest%20neighbor#networkx-algorithms-assortativity-average-degree-connectivity
    ANND_in_out=nx.average_degree_connectivity(net, source='in', target='out')
    ANND_out_in=nx.average_degree_connectivity(net, source='out', target='in')
    res['ANND_in_out']=ANND_in_out
    res['ANND_out_in']=ANND_out_in
    # assortativity: correlation_coefficient
    r_out_in = nx.degree_pearson_correlation_coefficient(net, x='out', y='in') 
    r_in_out = nx.degree_pearson_correlation_coefficient(net, x='in', y='out')
    res['pearson_out_in']=r_out_in
    res['pearson_in_out']=r_in_out
    # community  
    # -----------------
    #COMPARE WITH RANDOMIZED MODEL!! 
    # see garlaschelli
    # see pymnet.models.py: single_layer_conf()
    # also: nx.expected_degree_graph(degree_sequence)
    return res

def compare_agg_disagg_igraph():
    """
    Compare aggregate and disaggregate WTN 
    à la [Barigozzi et al. 09] Table III.
    """
    path = '/home/aurelien/local/data/comtrade/yrpc-2000.csv'
    mplex = read_ots_yrpc_data_igraph(path)
    path = '/home/aurelien/local/data/comtrade/yrp-2000.csv'
    g_agg = read_ots_yrp_data_igraph(path)
    dens_ra={}
    deg_in_ra={}
    # compute quantities in [Barigozzi et al. 09] II.D (p.5)
    for k,g in mplex.items() :
        # wij
        # density
        dens_ra[k]= mplex[k].density() /g_agg.density()
        # NSin/NDin https://igraph.org/python/doc/igraph.Graph-class.html
        deg_in_ra[k] = np.nan_to_num(ratio_ns_nd(mplex[k],mode='in')).mean()/np.nan_to_num(ratio_ns_nd(g_agg,mode='in')).mean()
        # NSout/NDout
        # WCCall
    # print
    list_barigozzi= [9,10,27,29,30,39,52,71,72,84,85,90,90,93]
    for i,k in enumerate(list_barigozzi):
        if i==0:
            print( 'lay\tdensity\tNSin/NDin ')
        if mplex.__contains__(str(k)): 
            print( '{}\t{}\t{}'.format(k,100*dens_ra[str(k)],deg_in_ra[str(k)] ) )
        else:
            print( '{}: NA'.format(k) )

def pipeline_igraph(path):
    """
    python3 -m pip install python-igraph
        
    generalized modularity measure
    [Mucha10] Mucha , Peter J., et al. "Community structure in time-dependent, multiscale,
    and multiplex networks." Science 328.5980 (2010): 876-878.
    (see overview in [Bianconi18] 2.6.3 p.26, 8.2.1 p.147)
    """
    #--------    
    #load :     https://igraph.org/python/doc/tutorial/tutorial.html#igraph-and-the-outside-world
    d=read_mplex_edge_file_igraph(path,directed=True, sep=',')
    d['0'].summary()
    # TODO: fast: dimacs, dl, edgelist, gml, graphdb, graphml, lgl, ncol, pajek   
    #--------    
    # compute monoplex stats (GraphBase)
    res_monoplex={}
    for k,g in d.items() :
        res_monoplex[k] = analyze_single_layer_igraph(g)  
    #--------     
    # plot monoplex 
    # https://igraph.org/python/doc/tutorial/tutorial.html#drawing-a-graph-using-a-layout
    #layout = g.layout("lgl")  # SLOW ; "fr" also
    layout = g.layout('kk') 
    g.vs["label"] = g.vs["name"]
    #g.vs["color"] = [color_dict[gender] for gender in g.vs["gender"]]
    #p=plot(g, layout = layout,target='comtrade.pdf')
    p=plot(g, layout = layout)
    p.show()
    #--------        
    # multiplex stats
    res_multiplex={}
    # !!!! rescaled link overlap
    # !!!! dendrogram
    #       https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html#scipy.cluster.hierarchy.linkage
    #       https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.dendrogram.html
    #       http://datanongrata.com/2019/04/27/67/
    #   community detection    
    #     https://github.com/vtraag/leidenalg/blob/d1f5efd230ca93cc50468892d65d518de423c494/src/Optimiser.py    
    #     multicd
    #     https://michaelsiemon.github.io/multiplexcd/
    #     https://michaelsiemon.weebly.com/community-detection-with-multiplexcd.html
    # Q: directed ???
    # https://github.com/rlaishra/MultiComSampling
    # -----------------
    # RANDOMIZED MODEL: MONOPLEX
    # random sampler with Degree_Sequence (see Vigier/Latapy https://arxiv.org/abs/cs/0502085)
    nsamp = 1000
    l=[]
    deg_in = g.degree(mode='in')
    deg_out = g.degree(mode='out')
    f=lambda g_: max(g_.pagerank())
    x0 = f(g)
    for n in range(nsamp):
        G=Graph.Degree_Sequence(deg_in,deg_out,method="no_multiple")#vl
        #print(g_.summary())
        #x = g.diameter()
        x = f(G)
        l.append(x)
    count,_,_=plt.hist(l,100)
    plt.plot([x0,x0],[0,max(count)],'red')
    plt.show()
    # random sampler Static_Fitness()
    # ?
    # -------------------
    # link with other igraph-based libs:
    # __graph_as_cobject()
    # https://lists.nongnu.org/archive/html/igraph-help/2015-07/msg00062.html
    # _get_py_capsule(graph): 
    # https://github.com/vtraag/louvain-igraph/blob/ec1c6e1fe10449fb4a2a8adaeb4cbe5c2d577126/src/VertexPartition.py
    pass


def pipeline_nx(path): 
    """
    backend = nx, https://networkx.github.io/documentation/stable
    """
    # -------------------------------------------
    # LOAD, get nx.MultiDiGraph
    directed = True
    (G,labels) = parse_multiplex_edges2(path,directed)
    
    # LAYER BY LAYER ANALYSIS
    layers = ['0']
    layers = [str(l) for l in range(10)]
    for layer in layers:
        G_DiG = get_DiG_from_MultiDiG(G,layer)
        res_monoplex = analyze_single_layer_nx(G_DiG,plot=False)
        plot_single_layer_nx(G_DIG,res_monoplex,plot_net=False,nit=10)


def pipeline_pymnet(path): 
    """
    """
    # LOAD
    mplex = read_mplex_edge_files(path,couplings='none',fullyInterconnected=True,directed=True,ignoreSelfLink=True)
    # -------------------------------------------
    # ANALYZE MONOPLEX: AGGREGATE GRAPH,  LAYER-BY-LAYER
    # see igraph, nx 
    """ nxwrap WRAPPER INCOMPLETE: MultiLayer is coded but not multiplex
    nodes = mplex.slices[0]
    sub_net = subnet(mplex, nodes, '0')
    nnx=nxwrap.MonoplexGraphNetworkxView(sub_net)
    ANND_in_out=nx.average_degree_connectivity(nnx, source='in', target='out')"""

    # -------------------------------------------
    # ANALYZE as MULTIPLEX
    """
    « degree, the clustering coefficient (...)
    distance-dependent properties such as the average distance, the cross-betweenness and the interdependence. » [bianconi18 §6,1]
    « the overlap [11, 13, 14] of the links in different layers, the 
    interdependence [12, 15] that extends the concept of betweenness centrality to multiplexes, or the centrality measures [16, 17]. » [bianconi/halu13]
    """
    # basic properties
    mplex['alb','0'].deg() 
    print( diagnostics.multiplex_density(mplex) )
    print( diagnostics.multiplex_degs(mplex,degstype="distribution") )
    # mplex properties
    res = analyze_pymnet(mplex)
    plot_pymnet(mplex)

    # -------------------------------------------
    # RANDOMIZED MODEL: MONOPLEX
    """ from pymnet:models.py    
        single_layer_conf:
        B.D McKay, N.C Wormald 'Uniform Generation of Random Regular Graphs of Moderate Degree'
        Journal of Algorithms 11, pages 52-67 (1990)
    """
        
    # -------------------------------------------
    # RANDOMIZED MODEL: MULTIPLEX
    """ from pymnet:models.py   
        conf_overlaps:
        Marceau, Vincent, et al. "Modeling the dynamical interaction between 
        epidemics on overlay networks." Physical Review E 84.2 (2011): 026105.
    """   
    # numérique  
    pymnet.models.conf_overlaps()    
    # analytique ?
    # normalized with NULL MODEL (GEMMETTO/GARLASCHELLI/Krantz )
    lo_glob_bin= res['lo_glob_bin']
    lo_resc= res['lo_glob_bin_resc']
    plot_overlap_mat(lo_glob_bin,lo_resc)
    # -------------------------------------------
    # RECONSTRUCT
    # see mastandrea14 ; garla17
    # see kleinenberg hyperbolic
    # link prediction nx:  https://networkx.github.io/documentation/stable/reference/algorithms/link_prediction.html#module-networkx.algorithms.link_prediction
    # -------------------------------------------
    # GET BACKBONE
    # see [Gemmetto18]
    # -------------
    # SPATIAL
    # see "Network Embedding visualization" 
    # http://www.pik-potsdam.de/~donges/pyunicorn/api/core/geo_network.html


########################################################
# PLOT

def plot_pymnet(mplex):
    """
    """
    # plot network
    webplot(mplex,'out.html')
    # get subnet
    #    intra neighbors of a node in a given layer
    nei = [neigh for neigh in mplex.A['0']['fra']]
    #    the associated subnet
    s= subnet(mplex,nei,'0')
    webplot(mplex,'out_neighbors.html')
    # ------------------
    # measures
    # bin
    link_glob_bin= res['link_glob_bin']
    deg_cor=res['deg_cor']
    plot_compare_overlap(deg_cor,link_glob_bin, lab=['deg_cor','link_glob_bin'])
    # wei

def plot_single_layer_nx(net,res,plot_net=False,nit=10) :   
    """
    """
    # prepare plotting
    forceatlas2 = ForceAtlas2(
                        # Behavior alternatives
                        outboundAttractionDistribution=True,  # Dissuade hubs
                        linLogMode=False,  # NOT IMPLEMENTED
                        adjustSizes=False,  # Prevent overlap (NOT IMPLEMENTED)
                        edgeWeightInfluence=1.0,
                        # Performance
                        jitterTolerance=1.0,  # Tolerance
                        barnesHutOptimize=True,
                        barnesHutTheta=1.2,
                        multiThreaded=False,  # NOT IMPLEMENTED
                        # Tuning
                        scalingRatio=2.0,
                        strongGravityMode=False,
                        gravity=1.0,
                        # Log
                        verbose=True)
    # plot net
    # https://networkx.github.io/documentation/stable/auto_examples/drawing/plot_unix_email.html#sphx-glr-auto-examples-drawing-plot-unix-email-py
    #pos = nx.spring_layout(agg_net, iterations=10)
    if plot_net: 
        pos = forceatlas2.forceatlas2_networkx_layout(net, pos=None, iterations=nit)
        try:
            nx.draw(net, pos, node_size=0, alpha=0.4, edge_color='r', font_size=16, with_labels=True)
            plt.show()
        except: 
            print('drawing FAILED')   
    # plot measures
    d=res['in_degree']   
    plt.hist( list(dict(d).values() ) ); 
    plt.xlabel('in_degree')
    plt.show()
    #
    d=res['out_degree']
    plt.hist( list(dict(d).values() ) );
    plt.xlabel('out_degree')
    plt.show() 
    #
    str_in = res['in_degree'] 
    plt.hist( list(dict(str_out).values() ) ) 
    plt.xlabel('str_out')
    plt.show() 
    # 
    clust= res['clustering'] 
    plt.hist( list(clust.values() ) ) 
    plt.xlabel('clustering')
    plt.show()
    #
    comp = res['component_strong']
    len_strong_conn_comp = [len(c) for c in comp]
    plt.hist( len_weak_conn_comp ) 
    plt.xlabel('length of weak_conn_comp')
    plt.show()
    #
    res['ANND_in_out']=ANND_in_out
    res['ANND_out_in']=ANND_out_in
    plt.subplot(1,2,1)
    plt.scatter(ANND_in_out.keys(),ANND_in_out.values(),color='k')	
    fs=18
    plt.xlabel('$k_{in}$',fontsize=fs); 
    plt.ylabel('$k_{out}^{nn}$',fontsize=fs)
    plt.subplot(1,2,2)
    plt.scatter(ANND_out_in.keys(),ANND_out_in.values(),color='k')	
    fs=18
    plt.xlabel('$k_{out}$',fontsize=fs); 
    plt.ylabel('$k_{in}^{nn}$',fontsize=fs)
    plt.show()
    #
    print('pearson {} {}'.format(res['pearson_in_out'],res['pearson_out_in']) )


########################################################
# DEPRECATED

"""
from py3plex.core import multinet
from py3plex.algorithms.statistics.basic_statistics import *
from py3plex.visualization.multilayer import *
from py3plex.algorithms.community_detection import community_wrapper as cw
from py3plex.visualization.colors import colors_default
from collections import Counter
"""

    
def pipeline_Py3plex(path): 
    """
    #https://github.com/SkBlaz/Py3plex/
    """
    # -------------------------------------------
    # LOAD, get nx.MultiDiGraph
    # bypass standard parsers
    directed = True
    (G,labels) = parse_multiplex_edges2(path,directed)
    mplex = multinet.multi_layer_network()
    mplex.core_network = G
    mplex.labels=labels
    # -------------------------------------------
    # ANALYZE MONOPLEX: aggregate network
    #agg_net = sub_net.aggregate_edges() ??
    # -------------------------------------------
    # ANALYZE MONOPLEX: layer-by-layer
    # get layer
    for l in ['0']:
        sub_net = mplex.subnetwork([l],subset_by="layers")
        agg_net = sub_net.aggregate_edges() 
        #sub_net.get_nx_object()
        # some properties
        sub_net.test_scale_free()
       
    # -------------------------------------------
    # ANALYZE MULTIPLEX
    # basic properties
    stats_frame = core_network_statistics(mplex)
    print(stats_frame)
    top_n_by_degree = identify_n_hubs(mplex.core_network,20)
    print(top_n_by_degree)
    # rank nodes
    #sparse_page_rank
    #(multipagerank)    
    # entanglement  github.com/renoust ?
    # -------------------------------------------
    # VIZ MULTIPLEX
    # with py3plex
    # mplex.visualize_network(show=True) # TOO SLOW
    # with nx # TOO MESSY
    #pos = nx.spring_layout(G, iterations=10)
    pos = forceatlas2.forceatlas2_networkx_layout(G, pos=None, iterations=2000)
    nx.draw(G, pos, node_size=0, alpha=0.4, edge_color='r', font_size=16, with_labels=True)
    plt.show()
    # -------------------------------------------
    # MULTIPLEX COMMUNITY
    #https://github.com/SkBlaz/Py3plex/blob/master/examples/example_community_detection.py
    partition = cw.louvain_communities(mplex)
    #print(partition)
    # select top n communities by size
    top_n = 10
    partition_counts = dict(Counter(partition.values()))
    top_n_communities = list(partition_counts.keys())[0:top_n]
    # assign node colors
    color_mappings = dict(zip(top_n_communities,[x for x in colors_default if x != "black"][0:top_n]))
    network_colors = [color_mappings[partition[x]] if partition[x] in top_n_communities else "black" for x in mplex.get_nodes()]
    # visualize the network's communities!
    hairball_plot(mplex.core_network,
                  color_list=network_colors,
                  layout_parameters={"iterations": 200},
                  scale_by_size=True,
                  layout_algorithm="force",
                  legend=False)
    plt.show()
