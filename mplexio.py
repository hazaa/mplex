# -*- coding: utf-8 -*-
"""

LIB: pymnet, nx, igraph ,( py3plex)

REFS:
https://networkx.github.io/documentation/stable
https://github.com/SkBlaz/Py3plex
https://igraph.org/python/#docs
    https://igraph.org/python/doc/igraph.GraphBase-class.html
http://cran.irsn.fr/web/packages/multinet/index.html
"""

import numpy as np
try:
	from pymnet import *
except:
	print("could not load pymnet")	
from igraph import *
import pandas as pd
try:
    import rpy2.robjects as robjects
    from rpy2.robjects import pandas2ri
except:
    print('could not load rpy2')

# -------------------------------------------
# HELPER FUNCTIONS (no backend)

def get_label_list(df):
    """
    """
    label_list = pd.unique(pd.concat((df[ 'reporter_iso'],df['partner_iso' ]))).tolist() 
    return sorted(label_list)

def relabel_df_str_to_int(df,label_list,lab=[ 'idx_reporter', 'idx_partner','product_code', 'export_value_usd', 'import_value_usd']  ):
    """
    relabel the dataframe read from ots csv data.
    Use int indices obtained from the (ordered) label_list.
    """
    df_lab_report = pd.DataFrame({'idx_reporter':range(len(label_list)),'reporter_iso': label_list })
    df_lab_partn = pd.DataFrame({'idx_partner':range(len(label_list)),'partner_iso': label_list })
    df_merge1 = pd.merge(df,df_lab_report, on='reporter_iso')
    df_merge2 = pd.merge(df_merge1,df_lab_partn, on='partner_iso')
    del df_merge1
    return df_merge2[lab ]
 
# -------------------------------------------
# HELPER FUNCTIONS (igraph)


def read_ots_yrpc_data_df(path):
    """
    !!!rpy2 doesn't work !!!
    
    see:    
        https://docs.tradestatistics.io/direct-download/yrpc-1962.rds
        
        in R:
        d = readRDS('yrpc-1962.rds')
    """
    readRDS = robjects.r['readRDS']
    df = readRDS(path)
    df = pandas2ri.rpy2py_dataframe(df)

def read_ots_yrp_data_igraph(path,directed=True,imports=True):
    """
    create an igraph from ots yrp data.  (product code is dropped)  
    "yrp": year reporter partner

    Parameters:
    -----------
    imports: bool
    Choose which to keep. (Trade data are reported twice, by exporter and importer). 

    Outputs:
    --------
    g: igraph.Graph

    see:    
        https://docs.tradestatistics.io/direct-download/yrpc-1962.rds
    this seems obsolete:
        https://github.com/tradestatistics/yearly-datasets/blob/master/00-scripts/02-dirs-and-files.R
        wget https://data.tradestatistics.io/06-tables/hs-rev2007/2-yrp/yrp-1962.csv.gz    
    """
    # read csv
    df = pd.read_csv(path)
    # dump the first column ('year')
    df = df[ [u'reporter_iso', u'partner_iso', u'export_value_usd', u'import_value_usd']]
    # set NaN to zero: use df.fillna instead
    df.fillna(0, inplace=True)
    # relabel
    #df_relab = relabel_df_str_to_int(df,label_list,lab=[ 'idx_reporter', 'idx_partner','export_value_usd', 'import_value_usd']  )
    if imports:
        # filter imports only 
        df_imp = df[df['import_value_usd']>0] 
        edge_set = df_imp[[ 'reporter_iso', 'partner_iso','import_value_usd']].to_numpy().tolist()
        # add edge set
        #https://igraph.org/python/doc/igraph.Graph-class.html#TupleList
        # TODO: !!!!!!! Graph.TupleList(df.itertuples(index=False), weights=True)
        g=Graph.TupleList(edge_set, directed=directed, weights=True)
    else: raise NotImplementedError
    # simplify
    #g.simplify(multiple=True, loops=True )
    return g  

def read_ots_yrpc_data_igraph(path,directed=True,imports=True,digit=2):
    """
    #create a dictionary where keys are layer labels and values are igraph.Graph
    #"yrpc": year reporter partner code    

    see:    
        https://docs.tradestatistics.io/direct-download/yrpc-1962.rds
    #seems obsolete :
    #    https://docs.tradestatistics.io/datasets.html
    #    wget https://data.tradestatistics.io/06-tables/hs-rev2007/1-yrpc/yrpc-2000.csv.gz

    #Parameters:
    #-----------
    #imports: bool
    #Choose which to keep. (Trade data are reported twice, by exporter and importer). 
    
    #Outputs:
    #--------
    #df: pd.DataFrame
    """
    df_grouped = agg_ots_yrpc_data(path,digit=digit)
    # first pass: get list of node labels, and list of layers
    label_list = get_label_list(df_grouped)
    layer_set = pd.unique(df_grouped['product_code']).tolist()
    df_relabel = relabel_df_str_to_int(df_grouped,label_list)
    # create dict of Graph objects
    d= {}
    for lay in layer_set: 
        # create graphs and vertices
        d[str(lay)]=Graph(directed=directed)
        d[str(lay)].add_vertices(label_list)
        # filter product code for that layer
        df_lay =   df_relabel[ df_relabel['product_code']==lay ] 
        if imports:
            # filter imports only 
            df_lay_imp = df_lay[df_lay['import_value_usd']>0] 
            edge_set = df_lay_imp[[ 'idx_reporter', 'idx_partner']].to_numpy().tolist()
            # add edge set
            d[str(lay)].add_edges(edge_set)
            d[str(lay)].es['weight'] = df_lay_imp['import_value_usd'].to_numpy().tolist()
        else:
            # filter exports only 
            df_lay_exp = df_lay[df_lay['export_value_usd']>0] 
            edge_set = df_lay_exp[[ 'idx_reporter', 'idx_partner']].to_numpy().tolist()
            # add edge set
            d[str(lay)].add_edges(edge_set)
            d[str(lay)].es['weight'] = df_lay_exp['export_value_usd'].to_numpy().tolist()
        # simplify multiple in undirected graph
        if directed == False:
            d[str(lay)].simplify(combine_edges=dict(weight="sum")) 
        # check multiples
        if np.sum(1*np.array(d[str(lay)].es.is_multiple())) >0:
            raise ValueError('multiple edge')
        # remove multiple edges, self-loops
        # DON'T USE SIMPLIFY IN THIS WAY, because weights are removed
        #d[str(lay)].simplify(multiple=True, loops=True )
    return d

""" 
def read_ots_yrpc_data_igraph(path,directed=True,imports=True):
   
    #create a dictionary where keys are layer labels and values are igraph.Graph
    #"yrpc": year reporter partner code    

    #see :
        #https://docs.tradestatistics.io/datasets.html
        #wget https://data.tradestatistics.io/06-tables/hs-rev2007/1-yrpc/yrpc-2000.csv.gz

    #Parameters:
    #-----------
    #imports: bool
    #Choose which to keep. (Trade data are reported twice, by exporter and importer). 
    
    #Outputs:
    #--------
    #df: pd.DataFrame
    
    df_grouped = agg_ots_yrpc_data(path)
    layer_set = pd.unique(df_grouped['product_code']).tolist()
    # first pass: get list of node labels, and list of layers
    # create dict of Graph objects
    d= {}
    for lay in layer_set: 
        # filter product code for that layer
        df_lay =   df_grouped[ df_grouped['product_code']==int(lay) ] 
        if imports:
            # filter imports only 
            df_lay_imp = df_lay[df_lay['import_value_usd']>0] 
            edge_set = df_lay_imp[[ 'reporter_iso', 'partner_iso','import_value_usd']].to_numpy().tolist()
            # add edge set
            d[str(lay)]= Graph.TupleList(edge_set, directed=directed, weights=True)
            # simplify multiple in undirected graph
            if directed == False:
                d[str(lay)].simplify(combine_edges=dict(weight="sum")) 
            # check multiples
            if np.sum(1*np.array(d[str(lay)].es.is_multiple())) >0:
                raise ValueError('multiple edge')
        else: raise NotImplementedError
        # remove multiple edges, self-loops
        #d[str(lay)].simplify(multiple=True, loops=True )
    return d
"""


def read_ots_data_igraph_slow(path,directed=True):
    """
    create a dictionary where keys are layer labels and values are igraph.Graph

    VERY SLOW

    see :
        https://docs.tradestatistics.io/datasets.html
        wget https://data.tradestatistics.io/06-tables/hs-rev2007/1-yrpc/yrpc-2000.csv.gz
    """
    raise DeprecationWarning('use read_ots_data_igraph() instead')
    df_grouped = agg_ots_data(path)
    # first pass: get list of node labels, and list of layers
    label_list = pd.unique(pd.concat((df_grouped[ 'reporter_iso'],df_grouped['partner_iso' ]))).tolist() 
    label_list = sorted(label_list)
    layer_set = pd.unique(df_grouped['product_code']).tolist()
    layer_set = [ str(l) for l in layer_set]
    # create dict of Graph objects
    d= {}
    for lay in layer_set: 
        d[lay]=Graph(directed=directed)
        d[lay].add_vertices(label_list)
    # loop through lines  
    for i in range(df_grouped.shape[0]):
        n1 = df_grouped.iat[i,0]#['reporter_iso'] 
        n2 = df_grouped.iat[i,1]#['partner_iso']
        if n1 != n2:
            lay = str(df_grouped.iat[i,2])#['product_code'] 
            exp = df_grouped.iat[i,3]#['export_value_usd'] 
            imp = df_grouped.iat[i,4]#['import_value_usd'] 
            g = d[lay]
            # https://igraph.org/python/doc/tutorial/tutorial.html#looking-up-vertices-by-names
            v1=g.vs.find(n1)
            v2=g.vs.find(n2)
            idx1 = v1.index
            idx2 = v2.index
            if exp>0:
                # create edge
                g[idx1,idx2]=1
                # set weight
                eid = g.get_eid(idx1,idx2)
                g.es[eid]["weight"] = int(exp)
            if imp>0:
                # create edge
                g[idx2,idx1]=1
                # set weight
                eid = g.get_eid(idx2,idx1)
                g.es[eid]["weight"] = int(imp)
    return d 

def read_mplex_edge_file_igraph(edgeinput,directed=True,ignoreSelfLink=True, sep=','):
    """read a multiplex file 
    
    (one-line header can be skipped)
    node1,node2,layer,weight
    node1,node2,layer,weight
    ....
    
    Parameters:
    ------------
    edgeinput: str
    
    Output:
    ------------
    dict: number of items = number of layers
    {"layer_name_1":igraph, ...}
    
    backend: igraph ,  https://igraph.org/python/#docs
    """

    # first pass: get list of node labels, and list of layers
    label_set = set()
    layer_set = set()
    edgefile=open(edgeinput,'r') if isinstance(edgeinput,str) else edgeinput
    skip=True
    for line in edgefile:
        if skip: skip=False
        else:
            n1,n2,lay,w=line.split(sep)
            label_set.add(n1)
            label_set.add(n2)
            layer_set.add(lay)
    edgefile.close()
    label_list = sorted(list(label_set))
    # create dict of Graph objects
    d= {}
    for lay in layer_set: 
        d[lay]=Graph(directed=directed)
        d[lay].add_vertices(label_list)
    # second pass: add nodes, add weights
    edgefile=open(edgeinput,'r') if isinstance(edgeinput,str) else edgeinput
    skip=True
    for line in edgefile:
        if skip: skip=False
        else:
            n1,n2,lay,w=line.split(sep)
            if n1!=n2 or not ignoreSelfLink:
                g = d[lay]
                # https://igraph.org/python/doc/tutorial/tutorial.html#looking-up-vertices-by-names
                v1=g.vs.find(n1)
                v2=g.vs.find(n2)
                idx1 = v1.index
                idx2 = v2.index
                # create edge
                g[idx1,idx2]=1
                # set weight
                eid = g.get_eid(idx1,idx2)
                g.es[eid]["weight"] = int(w)
    # check
    for lay in layer_set:
        if not d[lay].is_weighted(): raise ValueError('graph should be weighted')            
        if d[lay].is_directed() != directed: raise ValueError('graph should be directed')            
         
    edgefile.close()
    return d
"""
def read_edge_file_igraph(edgeinput,nlayer=1,directed=True,ignoreSelfLink=True, sep=','):
    A multiplex file 
    
    backend: igraph ,  https://igraph.org/python/#docs
    
    COPIED/MODIFIED from pymnet.netio.py
   
    l= []
    for n in range(nlayer): l.append(Graph(directed=directed))
    #layerfile=open(layerinput,'r') if isinstance(layerinput,str) else layerinput
    edgefile=open(edgeinput,'r') if isinstance(edgeinput,str) else edgeinput
    #nodefile=open(nodeinput,'r') if isinstance(nodeinput,str) else nodeinput
    skip=True
    for line in edgefile:
        if skip: skip=False
        else:
            n1,n2,lay,w=line.split(sep)
            if n1!=n2 or not ignoreSelfLink:
            #net[fi,ti,li]=w
            #net[li,fi,ti]=w
                #print(n1,n2,lay,w)
                #net[n1,lay][n2,lay]=int(w)
                g = l[int(lay)]
                g.add_vertices([n1,n2]) # SHOULD BE OPTIMIZED
                g[n1,n2] = int(w)
    return l
"""


# -------------------------------------------
# HELPER FUNCTIONS (NETWORKX)

def parse_multiplex_edges2(input_name,directed,skip1st=True):
    """
    backend = nx
    copied from py3plex/core/parsers.py
    changed index order, and separation character
    """
    if directed:
        G = nx.MultiDiGraph()
        
    else:
        G = nx.MultiGraph()

    i0,i1,i2,i3 = 2,0,1,3 
    unique_layers = set() 
    skip = skip1st   
    with open(input_name) as ef:
        for line in ef:
            if skip: skip=False
            else:
                parts = line.strip().split(",")
                node_first = str(parts[i1])
                node_second = str(parts[i2])
                layer = parts[i0]
                if len(parts) > 2:
                    weight = parts[i3]
                else:
                    weight = 1
                G.add_node((node_first,str(layer)))
                G.add_node((node_second,str(layer)))
                unique_layers.add(str(layer[0]))
                G.add_edge((node_first,str(layer)),(node_second,str(layer)),weight=float(weight))#,type="default")

    return (G,None)

def get_DiG_from_1layer_MultiDiG(net):
    """
    get DiGraph from a MultiDiG with 1 layer
    
    https://networkx.github.io/documentation/stable/reference/classes/multidigraph.html
    """
    g = nx.DiGraph()
    for node in net.nodes():
        g.add_node(node[0])
    
    for u, v,  data in net.edges(data=True): 
        g.add_edge(u[0], v[0], weight=data['weight'])
    return g

def get_DiG_from_MultiDiG(net,layer):
    """
    get DiGraph from a MultiDiG with several layers
    
    https://networkx.github.io/documentation/stable/reference/classes/multidigraph.html
    """
    g = nx.DiGraph()
    for node in net.nodes():
        if node[1]==layer: g.add_node(node[0])
    
    for u, v,  data in net.edges(data=True): 
        if u[1]==layer and v[1]==layer:
            g.add_edge(u[0], v[0], weight=data['weight'])
    return g    
    
# -------------------------------------------
# HELPER FUNCTIONS (PYMNET)

def agg_ots_yrpc_data(path,digit=2):
    """
    read and aggregate 4-digit OTS files (yrpc) to 2-digit
    "yrpc": year reporter partner code

    see :
      https://docs.tradestatistics.io/datasets.html
      wget https://data.tradestatistics.io/06-tables/hs-rev2007/1-yrpc/yrpc-2000.csv.gz


    Input:
    -----
    path: str

    Output:
    -------
    df: pd.DataFrame

    Example input file:
    -------------------

    year,reporter_iso,partner_iso,product_code,export_value_usd,import_value_usd
    2000,afg,aia,0808,126,
    2000,afg,and,3004,21,
    2000,afg,and,5701,30569,
    2000,afg,and,5702,3431,
    2000,afg,are,0713,2774,42206

    """
    df = pd.read_csv(path)
    # dump the first column
    df = df[ [u'reporter_iso', u'partner_iso', u'product_code',
       u'export_value_usd', u'import_value_usd']]
    # set NaN to zero: use df.fillna instead
    df.fillna(0, inplace=True)
    # replace 4-digit index by 2-digit
    df['product_code'] = df['product_code'].apply(lambda x: int(str(x)[0:digit]))
    # Groupby
    #https://pandas.pydata.org/pandas-docs/stable/user_guide/groupby.html#aggregation
    return df.groupby([u'reporter_iso', u'partner_iso','product_code']).sum().reset_index()
    

def read_ots_data_pymnet(path,couplings='none',directed=True,fullyInterconnected=True):
    """
    https://docs.tradestatistics.io/datasets.html
    wget https://data.tradestatistics.io/06-tables/hs-rev2007/1-yrpc/yrpc-2000.csv.gz

    """
    df_grouped = agg_ots_data(path)
    # create mplex
    net=MultiplexNetwork(couplings=[couplings],fullyInterconnected=fullyInterconnected,directed=directed)
    # loop through lines
    for i in range(df_grouped.shape[0]):
        n1 = df_grouped.iat[i,0]#['reporter_iso'] 
        n2 = df_grouped.iat[i,1]#['partner_iso']
        if n1 != n2:
            lay = str(df_grouped.iat[i,2])#['product_code'] 
            exp = df_grouped.iat[i,3]#['export_value_usd'] 
            imp = df_grouped.iat[i,4]#['import_value_usd'] 
            if exp>0:
                net[n1,lay][n2,lay]=exp
            if imp>0:
                net[n2,lay][n1,lay]=imp
    return net 

def read_edge_files():
    """
    equivalent of pymnet.write_edge_file
    """
    pass

def read_mplex_edge_files(edgeinput,couplings='categorical',fullyInterconnected=True,directed=False,ignoreSelfLink=True, sep=','):
    """A multiplex file separed into files for layers, edge and nodes.
    
    backend: pymnet
    
    COPIED/MODIFIED from pymnet.netio.py
    """
    net=MultiplexNetwork(couplings=[couplings],fullyInterconnected=fullyInterconnected,directed=directed)
    #layerfile=open(layerinput,'r') if isinstance(layerinput,str) else layerinput
    edgefile=open(edgeinput,'r') if isinstance(edgeinput,str) else edgeinput
    #nodefile=open(nodeinput,'r') if isinstance(nodeinput,str) else nodeinput
    skip=True
    for line in edgefile:
        if skip: skip=False
        else:
            n1,n2,lay,w=line.split(sep)
            if n1!=n2 or not ignoreSelfLink:
            #net[fi,ti,li]=w
            #net[li,fi,ti]=w
                #print(n1,n2,lay,w)
                net[n1,lay][n2,lay]=int(w)
    return net

def threshold2(net,threshold,method=">=",ignoreCouplingEdges=False):
    """
        COPIED/MODIFIED from pymnet.transforms.py
    """
    def accept_edge(weight,threshold,rule):
        if rule==">=":
            return weight>=threshold
        elif rule=="<=":
            return weight<=threshold
        elif rule==">":
            return weight>threshold
        elif rule=="<":
            return weight<threshold
        else:
            raise Exception("Invalid method for thresholding: "+str(rule))

    mplex=(type(net)==MultiplexNetwork)
    if mplex:
        for coupling in net.couplings:
            if coupling[0]!="none":
                mplex=False
            

    if mplex:
        newNet=MultiplexNetwork(couplings=net.couplings,
                                directed=net.directed,
                                noEdge=net.noEdge,
                                fullyInterconnected=net.fullyInterconnected)
    else:
        newNet=MultilayerNetwork(aspects=net.aspects,
                                 noEdge=net.noEdge,
                                 directed=net.directed,
                                 fullyInterconnected=net.fullyInterconnected)

    #copy nodes,layers,node-layers
    for node in net:
        newNet.add_node(node)
    for aspect in range(net.aspects):
        for layer in net.slices[aspect+1]:
            newNet.add_layer(layer,aspect=aspect+1) 
    if not net.fullyInterconnected:
        for nodelayer in net.iter_node_layers():
            #layer=lnodelayer[1:] #BUG AH
            layer=nodelayer[1:]
            if net.aspects==1:
                layer=layer[0]
            newNet.add_node(nodelayer[0],layer=layer)

    if mplex:
        for layer in net.iter_layers():
            for edge in net.A[layer].edges:
                if accept_edge(edge[-1],threshold,rule=method):
                    newNet.A[layer][edge[0]][edge[1]]=1
    else:
        for edge in net.edges:
            if accept_edge(edge[-1],threshold,rule=method):
                newNet[edge[:-1]]=1
    return newNet
