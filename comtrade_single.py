# -*- coding: utf-8 -*-
"""
Reproduce the script in [dew].
Create a graph from a single commodity csv file.


refs:
	http://www.mkivela.com/pymnet/
	[dew] https://briandew.wordpress.com/2016/06/15/trade-network-analysis-why-centrality-matters/
	https://comtrade.un.org/data/Doc/api/ex

data:
	https://comtrade.un.org/data
	3. Select desired data
		Trade flows=export
		Reporters = All 
		Partners = All
		HS2002 commodity codes : 760200 (aluminium waste and scrap)

PROBLEME: full (bulk) data needs subscription !
	
some third-party packages:
	https://github.com/ejokeeffe/tradedownloader/blob/master/tradedownloader/utilities.py
	https://github.com/danieljschmidt/UN-Comtrade-Download/blob/master/Example.ipynb
	https://www.open.edu/openlearn/ocw/mod/oucontent/view.php?id=83251&section=1.4
"""

"""
import requests
url='https://comtrade.un.org/api/get?max=500&type=C&freq=A&px=HS&ps=2018&r=152&p=all&rg=all&cc=851712'
un_data=requests.get(url)
print(un_data.content)"""


import networkx as nx
import csv
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable

# ----------------------------------------
# CREATE GRAPH FROM CSV FILE

ROOT_DIR="/home/aurelien/local/data/comtrade/"

G = nx.DiGraph() # create a directed graph called G
 
# Loop reads a csv file with scrap aluminum bilateral trade data
# https://docs.python.org/3/library/csv.html
with open('{}760200_2012_restrict.csv'.format(ROOT_DIR), 'r') as csvfile:
     csv_f = csv.reader(csvfile)
     csv_f.next()
# Now we build the network by adding each row of data 
# as an edge between two nodes (columns 1 and 2).
     for row in csv_f:
        print(row)
        if len(row[10])>0 and len(row[13])>0: 
			G.add_edge(row[10],row[13],weight=row[27])

x = G['USA']['GBR']['weight']
print('USA 2012 scrap aluminum exports to UK, in kg: {}'.format(x))
# 
#G.remove_node('')

# ----------------------------------------
# Calculate eigenvector centrality of matrix G 
# with the exports value as weights
ec = nx.eigenvector_centrality_numpy(G, weight='weight')
# Set this as a node attribute for each node
nx.set_node_attributes(G, name='cent', values=ec)
# Use this measure to determine the node color in viz
node_color = [float(G.node[v]['cent']) for v in G]

# ----------------------------------------
# Blank dictionary to store total exports
totexp = {}

# Calculate total exports of each country in the network
for exp in G.nodes(): 
     tx=sum([float(g) for exp,f,g in G.out_edges(exp, 'weight')])
     totexp[exp] = tx
#??     avgexp = np.mean(tx)
val = np.array(totexp.values())
avgexp =0.05*np.mean(val)
nx.set_node_attributes(G, name='totexp', values=totexp)

# Use the results later for the node's size in the graph
node_size = [float(G.node[v]['totexp']) / avgexp for v in G]
# ----------------------------------------
# Visualization
# Calculate position of each node in G using networkx spring layout
pos = nx.spring_layout(G,k=30,iterations=8) 
# Draw nodes
nodes = nx.draw_networkx_nodes(G,pos, node_size=node_size, \
                               node_color=node_color, alpha=0.5) 
# Draw edges
edges = nx.draw_networkx_edges(G, pos, edge_color='gray', \ #'lightgray'
                               arrows=False, width=0.5,)
# Add labels
nx.draw_networkx_labels(G,pos,font_size=5)
nodes.set_edgecolor('gray')
# Add labels and title
plt.text(0,-0.1, \
         'Node color is eigenvector centrality; \
         Node size is value of global exports', \
         fontsize=7)
plt.title('Scrap Aluminum trade network, 2012', fontsize=12)
# Bar with color scale for eigenvalues
cbar = plt.colorbar(mappable=nodes, cax=None, ax=None, fraction=0.015, pad=0.04)
cbar.set_clim(0, 1)
# Plot options
plt.margins(0,0)
plt.axis('off')
# Save as high quality png
#plt.savefig('760200.png', dpi=1000)
plt.show()
