# -*- coding: utf-8 -*-
"""
Measure multiplex networks

REF:
   [Gemmetto et al. 16] "Multiplexity and multireciprocity in directed multiplexes"
                            arxiv: 1411.1282
   [Gemmetto 18] "On metrics and models for multiplex networks"                  
					http://hdl.handle.net/1887/61132
   [Bianconi 18] Bianconi "Multilayer networks. Structure and function", OUP, 2018.

   [Halu 13] Halu, Mukherjee, Bianconi 2013 http://arxiv.org/abs/1309.4411v3
   [SG11] Squartini, Garlaschelli "Analytical maximum-likelihood method to detect patterns in real networks" arxiv:1103.0701

LIB: pymnet, nx, igraph ,( py3plex)
    https://networkx.github.io/documentation/stable
    https://igraph.org/python/#docs
    http://mkivela.com
    (https://github.com/SkBlaz/Py3plex)
    (http://cran.irsn.fr/web/packages/multinet/index.html)

"""

import numpy as np
import pandas as pd
try:
    from pymnet import *
    from pymnet.transforms import subnet,threshold,aggregate 
except:
    print('pymnet could not be loaded')   

from igraph import *

import itertools

from functools import reduce

import matplotlib.pyplot as plt
import warnings

from cm_ml.cmtools import solve_eq_UBCM,pij_UBCM,solve_eq_UWCM,pij_UWCM
from scitools.tools import logfit

from mplex.tools import *

#######################################################
# BACKEND-INDEPENDANT FUNCTIONS

def overlap2mat(d):
    """
    get a symmetric matrix from doubly-indexed overlap dict
        d={('1', '0'): 512,
           ('1', '2'): 545,
           ('1', '3'): 541, ...}
    """
    # get unique labels
    lab = sorted(set([ key_[0] for key_ in d.keys() ]))
    n = len(lab)
    # create matrix
    mat = np.zeros((n,n))
    for i,lab_i in enumerate(lab):
        for j,lab_j in enumerate(lab):
            try: mat[i,j]=d[lab_i,lab_j]
            except KeyError: pass
    # add the transpose
    return mat+mat.transpose()

def link_overlap_normalize(lo,nlink):
    """
    compute $ 2. \sum_{i<j} min{a_{ij}^\alpha a_{ij}^\beta} / (L^\alpha+L^\beta)$
    from $ \sum_{i<j} min{a_{ij}^\alpha a_{ij}^\beta} $

    """
    lo_nor = {}
    for l in lo.keys():
        l1,l2 = l
		# see [Gemmetto 18] eq.(1.20-22)
        denominator =  nlink[l1] + nlink[l2] 
        lo_nor[l] = 2*lo[l]/denominator
    return lo_nor 

def link_overlap_average(nlink,d_pipj,model='UBCM'):
    """
    Compute the average of link overlap à la [Gemmetto et al. 16].
    Formulas:
        UBCM: [Gemmetto 18] eq.(1.20), (1.22)
    """
    d_avg = {}
    layers = d_pipj.keys()
    for layer_comb in itertools.combinations(layers, 2):
        l1,l2 = sorted(layer_comb)
        if model=='UBCM':
			# see [Gemmetto 18] eq.(1.20-22)
            denominator =  nlink[l1] + nlink[l2] 
            # $\sum_{i<j} p_{ij}^\alpha  p_{ij}^\beta   $
            sumprod_pij = np.sum(np.tril(d_pipj[l1]*d_pipj[l2]))
            d_avg[l1,l2] = 2* sumprod_pij/denominator
        elif model=="UWCM":
            # see [Gemmetto 18]  eq.(1.48)   
            denominator =  nlink[l1] + nlink[l2]  # is strength !!
            tril = np.tril(d_pipj[l1]*d_pipj[l2])
            sumprod_pij = np.sum( tril /(1-tril) )
            d_avg[l1,l2] = 2* sumprod_pij/denominator
        else: 
            raise NotImplementedError()         
    return d_avg 

def link_overlap_std(nlink,d_pipj, model='UBCM'):
    """ 
    Compute the std of link overlap.
    Formulas: 
        UBCM: [Gemmetto 18] (1.27)   WARNING: not the same as eq.(1.31)
    """
    d_std = {}
    layers = d_pipj.keys()
    for layer_comb in itertools.combinations(layers, 2):
        l1,l2 = sorted(layer_comb)
        if model=='UBCM':
            prod_ =  d_pipj[l1]* d_pipj[l2]                
            sumprod_pij = np.sum( np.tril( prod_-prod_**2 ) )
            denominator =  nlink[l1] + nlink[l2]
            d_std[l1,l2] = np.sqrt(sumprod_pij) / denominator 
        else: 
            raise NotImplementedError()         
    return d_std


def link_overlap_rescale(lo,lo_avg,layers=None,binary=True):
    """
    Normalize the link overlap score in the multiplex network, 
    à la [Gemmetto et al. 16].
    Formulas:
        UBCM: [Gemmetto 18] eq.(1.19)
    """
    lo_resc = {}
    if layers is None: layers = lo.keys()
    for l in layers:
        # check for index inversion: lo_avg[(li,lj)]<-lo_avg[(lj,li)]
        l1,l2=l
        avg = lo_avg[l1,l2]
        #if lo_avg.__contains__(l): avg = lo_avg[l1,l2]
        #else : avg = lo_avg[l2,l1]
        # rescale
        lo_resc[l] = (lo[l] - avg)/(1. - avg)
    return lo_resc

def link_overlap_zscore(lo_nor, lo_avg, lo_std,layers=None):
    """ 
    Compute the zscore of link overlap.
    Formulas: 
        UBCM: [Gemmetto 18] eq.(1.32)
    """
    lo_zscore = {}
    if layers is None: layers = lo_nor.keys()
    for l in layers:
        # check for index inversion: lo_avg[(li,lj)]<-lo_avg[(lj,li)]
        l1,l2=l
        avg = lo_avg[l1,l2] 
        std = lo_std[l1,l2]
        #if lo_avg.__contains__(l): avg = lo_avg[l1,l2]
        #else : avg = lo_avg[l2,l1]
        #if lo_std.__contains__(l): std = lo_std[l1,l2]
        #else : std = lo_std[l2,l1]
        # z-score
        lo_zscore[l] = (lo_nor[l] - avg)/std
    return lo_zscore



#######################################################
# FUNCTIONS WITH SUPPORT FOR SEVERAL BACKENDS 

def get_layers(mplex):
    """
    """
    if isinstance(mplex,MultiplexNetwork):
        return mplex.slices[1]
    elif isinstance(mplex,dict): return mplex.keys()
    else: raise ValueError('unknown backend')


def fit_UBCM(mplex):
    """
    fit a UBCM for each layer independently
    """
    if isinstance(mplex,MultiplexNetwork):
        return fit_UBCM_pymnet(mplex)
    elif isinstance(mplex,dict):
        return fit_UBCM_igraph(mplex)    
    else: raise ValueError('unknown backend')

def fit_UWCM(mplex):
    """
    fit a UWCM for each layer independently
    """
    if isinstance(mplex,dict):
        return fit_UWCM_igraph(mplex)    
    else: raise ValueError('unknown backend')

def link_overlap(mplex,layers,binary=True, directed=False,output='dict'):
    """
    Call the relevant link_overlap function depending on the backend,
    for every layer combination.
        
    Ref: ?
    """    
    if output=='dict':
        d_out = {}     
    elif output=='matrix':
        n = len(layers)
        d_out = np.zeros((n,n))
        
    for layer_comb in itertools.combinations(layers, 2):
        l1,l2 = sorted(layer_comb)
        if isinstance(mplex,MultiplexNetwork):
            count = link_overlap_pairwise_pymnet(mplex,l1, l2,binary=binary, directed=directed)
        elif isinstance(mplex,dict):
            count= link_overlap_pairwise_igraph(mplex[l1],mplex[l2],binary=binary, directed=directed)
        else: raise ValueError('unknown backend')
        if output=='dict':
            d_out[(l1,l2)] = count             
        elif output=='matrix':    
            i = layers.index(l1)
            j = layers.index(l2)
            d_out[i,j]=count
    return d_out 

def compare_real_CM_igraph(mplex,d_pipj,model="UBCM",plot=True,threshold=None):
    """ 
    https://igraph.org/python/doc/igraph.GraphBase-class.html#Adjacency
    https://igraph.org/python/doc/igraph.GraphBase-class.html#Weighted_Adjacency
    https://igraph.org/python/doc/igraph.GraphBase-class.html#transitivity_local_undirected
    REFS:
    [SG11]
    """
    for key in mplex.keys():
        g = mplex[key]
        P = d_pipj[key]
        # threshold values of P
        if not threshold is None: raise NotImplementedError
        # build graph from P  
        # NB: even for UBCM, a weighted adjacency matrix is created
        if model=="UBCM":
            g_CM = Graph.Weighted_Adjacency(P.tolist(), mode=ADJ_UNDIRECTED, attr="weight", loops=False)
        else:  raise NotImplementedError       
        # add names
        # compute functions
        if model=="UBCM":
            k = g.degree()
            k_cm = P.sum(axis=0)
            trans_real = g.transitivity_local_undirected()
            trans_CM = g_CM.transitivity_local_undirected()
            if plot:
                plt.scatter(k,trans_real,'r')
                plt.scatter(k_cm,trans_CM,'b')
                plt.show()
        elif model=="UWCM":    
            stren = np.array(g.strength(weights='weight'))
            stren_sum = np.sum(stren)
            trans_real = g.transitivity_local_undirected()
            trans_CM = g_CM.transitivity_local_undirected()

        elif model=="DWCM": 
            #wei = np.array(g.strength(mode="in",weights='weight'))
            raise NotImplementedError
        else:  raise NotImplementedError   
            
#######################################################
# PYMNET FUNCTIONS

def get_sorted_degree(mplex,layer):
    """
    get the degree sequence for a given layer,
    sorted by node label.
    """
    assert isinstance(mplex,MultiplexNetwork)

    nodes = mplex.slices[0]
    d=degs(subnet(mplex, nodes, layer),'nodes')
    d_sorted = sorted(d.items(), key=lambda kv: kv[0], reverse=False)
    return [d[1] for d in d_sorted]
            

def fit_UBCM_pymnet(mplex):
    """
    fit a UBCM for each layer independently
    """
    assert isinstance(mplex,MultiplexNetwork)

    layers = mplex.slices[1]
    d_pipj = {}
    for layer in layers:
        # get degree sequence
        k= get_sorted_degree(mplex,layer)
        k_ar=np.array( k ,dtype=np.int32 ) 
        # solve for Pij under UBCM model
        sol = solve_eq_UBCM(k_ar)	
        if sol.success:
            P = pij_UBCM(sol.x)
            d_pipj[layer] = P
        else: raise RuntimeError('solve_eq_UBCM did not converge') 
    return d_pipj 


def degree_correlation(mplex,output='dict'):
    """
    see [Bianconi18] p.132
    """
    assert isinstance(mplex,MultiplexNetwork), "pymnet.MultiplexNetwork required"
    
    nodes = mplex.slices[0]
    layers = mplex.slices[1]
    n_n=len(nodes)
    n_l=len(layers)

    if output=='dict':
        d_out = {}        
    elif output=='array':    
        mat = np.zeros((n_l,n_l))
    else: return ValueError('unknown output type')    
    # compute <k^{\alpha}> 
    ki={}
    sigmai={}
    for layer in layers:
       # get distribution of degrees 
       d_ = degs(subnet(mplex, nodes, layer),'distribution') 
       # compute weighted average
       m = map(lambda x:x[0]*x[1]  ,d_.items()) # get key * value
       ki[layer]= float(reduce( lambda x,y:x+y, m ))/float(n_n) #average over key * value 
       # compute sigma_{\alpha}
       d_ = degs(subnet(mplex, nodes, layer),'nodes').values()
       m = map( lambda x: (x-ki[layer])**2 ,d_)
       sigmai[layer]= float(reduce( lambda x,y:x+y, m ))/float(n_n)
    
    # compute <k^{\alpha} k^{\beta}>
    for layer_comb in itertools.combinations(layers, 2):
        i,j = layer_comb
        # TODO: SHOULD BE SORTED HERE
        di = np.array(get_sorted_degree(mplex,i))
        dj = np.array(get_sorted_degree(mplex,j))
        kikj = np.mean(di*dj)
        cor = (kikj - ki[i]* ki[i])/(sigmai[i]*sigmai[j])
        if output=='dict':
            d_out[(i,j)] = cor        
        elif output=='array':    
            mat[int(i),int(j)]=cor
    if output=='dict':
        return d_out    
    elif output=='array':            
        return mat +mat.transpose() 

        

"""
def link_overlap_average(d,d_pipj,model='UBCM'):
    #compute the average of link overlap à la [Gemmetto et al. 16].
    #
    #formula: [Gemmetto 18] eq.(1.20), (1.22)
   
    assert isinstance(mplex,MultiplexNetwork), "pymnet.MultiplexNetwork required"
    d_avg = {}
    n_n=len(mplex.slices[0])
    layers = mplex.slices[1]
    n_l=len(layers)
    nlink = multiplex_nlink(mplex)

    #if normalize=="max":
	#	normal = n_n*(n_n-1)
	
    for layer_comb in itertools.combinations(layers, 2):
        l1,l2 = layer_comb 
        if model=='UBCM':
			# see [Gemmetto 18] eq.(1.20-22)
            denominator =  nlink[l1] + nlink[l2]     
            # $\sum_{i<j} p_{ij}^\alpha  p_{ij}^\beta   $
            sumprod_pij = np.sum(np.tril(d_pipj[l1])*np.tril(d_pipj[l2]))
            d_avg[l1,l2] = 2* sumprod_pij/denominator
        else: 
            raise NotImplementedError()         
    return d_avg         """

def link_overlap_pairwise_pymnet(mplex,l1, l2,binary=True, directed=False):
    """
    compute $ \sum_{i<j} min{a_{ij}^\alpha a_{ij}^\beta} $

    """
    count = 0 
    for e in mplex.A[l1].edges:
        ni,nj,w1 = e
        w2 = mplex.A[l2][ni,nj]        
        m = min(w1,w2)
        if binary and m>0: m=1
        count += m
    return count            
            
def link_overlap_old(net,output='dict',normalize=False):
    """
    compute global and local overlap
    O^{\alpha,\alpha'} = \sum_{i<j} a_{ij}^\alpha a_{ij}^{\alpha'}
    
    Ref: ?

    Parameters
    ----------
    net : pymnet.MultiplexNetwork
       The original network with n_l layers.

    Returns
    -------
    mat: matrix, shape=(n_l,n_l)
    the global overlap
    
    d: dict
    the local overlap    
    
    """
    assert isinstance(net,MultiplexNetwork), "pymnet.MultiplexNetwork required"

    ol_degs = {}
    nodes = net.slices[0]
    layers = net.slices[1]
    n_n=len(nodes)
    n_l=len(layers)
    if normalize:
        nlink_max = n_n*(n_n-1)/2.
    else:    nlink_max =1.
    net0 = subnet(net, nodes, layers) # ??   
    if output=='dict':
        d_out = {}        
    elif output=='array':    
        mat = np.zeros((n_l,n_l))
    else: return ValueError('unknown output type')    

    for layer_comb in itertools.combinations(layers, 2):
        # get subnet
        sub_net = subnet(net0, nodes, layer_comb)
        # set nonzero weights to 1 
        thr_val=0
        thr_net = threshold2(sub_net, thr_val,method=">")
        # aggregate 2 layers on 1 layer
        agg_net = aggregate(thr_net, 1)
        # threshold to keep only weight >=2, == edges present on both layers  
        thr_val=2
        thr_net2 = threshold2(agg_net, thr_val,method=">=")
        # count edges 
        count = 0
        for e in thr_net2.edges:
            count +=1 
        # save 
        i,j = layer_comb
        if output=='dict':
            d_out[(i,j)] = count / nlink_max
        elif output=='array':    
            mat[int(i),int(j)] = count / nlink_max
            
    if output=='dict':
        return d_out, degs(thr_net2, 'nodes')     
    elif output=='array':            
        return mat +mat.transpose() , degs(thr_net2, 'nodes') 

#######################################################
# IGRAPH FUNCTIONS

# basic stats 
def plot_local_basic_stats_ig(bs,layer,directed=True,axes=None):
    """
    plot local graph stats
    """
    l = layer
    if axes is None:
        fig, axes = plt.subplots(2, 2,figsize=(12, 12))
    if directed:
        # s=f(k), cf Kaluza10 Fig.2
        sin=bs[l]['stren_in']
        logfit(kin,s,ax=axes[0][0], fun='pow',xlabel='k_in',ylabel='s_in')
        # cluster
        axes[1][0].scatter(bs[l]['deg_in'], bs[l]['trans'])
        axes[1][0].set_xlabel('deg_in')
        axes[1][0].set_ylabel('trans')
    else:
        # s=f(k), cf Kaluza10 Fig.2
        k=bs[l]['deg']
        s=bs[l]['stren']
        logfit(k,s,ax=axes[0][0], fun='pow',xlabel='$k_i$',ylabel='$s_i$')
        # knn
        axes[1][0].scatter(bs[l]['deg'], bs[l]['knn'])
        axes[1][0].set_xlabel('$k_i$')
        axes[1][0].set_ylabel('$k^{nn}_i$')
        # cluster
        axes[1][1].scatter(bs[l]['deg'], bs[l]['trans'])
        axes[1][1].set_xlabel('$k_i$')
        axes[1][1].set_ylabel('$c_i$')        
    # shortest path
    #avg_shortest_path = [ np.nanmean(np.array(li_)[np.isfinite(li_)]) for li_ in bs[l]['shortest']]
    #axes[2][1].hist( avg_shortest_path )  
    #axes[2][1].set_xlabel('avg_shortest_path')
    #axes[2][1].set_ylabel('freq') 
    return fig,axes 
 
def print_global_basic_stats_ig(r):
    """
    TODO: multi-index
    """
    """
    for layer,d in r.items():
        print(layer)
        for stat_name,stat_value in d.items():
            #if np.isscalar(stat_value):
            if isinstance(stat_value,float):
                print(stat_name)
                print(stat_value)
                #df =pd.DataFrame( {'stat name':stat_name, 'value':stat_value});print(df) # FAILS
    """
    pass           
    
    
def basic_stats_ig(mplex,directed=True):
    """ compute network stats
    https://igraph.org/python/doc/igraph.GraphBase-class.html#transitivity_local_undirected

    """
    r={}
    for k,g in mplex.items():
        g = mplex[k]
        r[k]={}
        if isinstance(g,Graph):
            r[k]['dens']= g.density() #scalar
            #= clustering
            r[k]['trans']= g.transitivity_local_undirected() 
            r[k]['trans_avg']= np.nanmean(r[k]['trans'])
            r[k]['evcent']= g.evcent(directed=directed)
            r[k]['between']= g.betweenness(directed=directed)
            r[k]['shortest']=g.shortest_paths()
            shortest = np.array(r[k]['shortest']).flatten()
            r[k]['shortest_avg']=np.mean(shortest[np.isfinite(shortest)])
            r[k]['shortest_max']=np.max(shortest[np.isfinite(shortest)])
            if directed:
                r[k]['deg_in']=np.array(g.indegree())
                r[k]['deg_out']=np.array(g.outdegree())
                r[k]['deg_in_avg']=np.mean(r[k]['deg_in'])
                r[k]['deg_out_avg']=np.mean(r[k]['deg_out'])
                r[k]['stren_in'] = np.array(g.strength(mode='in',weights='weight'))
                r[k]['stren_out'] = np.array(g.strength(mode='out',weights='weight'))
                r[k]['stren_in_avg'] = np.mean(r[k]['stren_in'])
                r[k]['stren_out_avg'] = np.mean(r[k]['stren_out'])
            else:  
                r[k]['deg']=np.array(g.degree())
                r[k]['deg_avg']=np.mean(r[k]['deg'])  
                r[k]['stren'] = np.array(g.strength(weights='weight'))
                r[k]['stren_avg'] = np.mean(r[k]['stren'])
                r[k]['knn']=np.array(g.knn()[0])
            # indegree is already computed above :
            r[k]['NSin_NDin']= np.nan_to_num(ratio_ns_nd(g,mode='in')).tolist()
            r[k]['NSout_NDout']=np.nan_to_num(ratio_ns_nd(g,mode='out')).tolist()
            if g.is_directed(): 
                #https://igraph.org/python/doc/igraph.Graph-class.html#dyad_census
                # asymmetric (there is an edge from a to b or from b to a but not the other way round) 
                r[k]['dyad_census'] = g.dyad_census() 
                r[k]['reciprocity'] = g.reciprocity() 
                # ? reciprocity() instead
        #NSin_NDin[k]= np.nan_to_num(ratio_ns_nd(g,mode='in')).mean()
        #NSout_NDout[k]=np.nan_to_num(ratio_ns_nd(g,mode='out')).mean()
        # Gini ?
    """        
    r= {'dens':dens, 'trans':trans, 'evcent':evcent,'between':between,
        'deg_in':deg_in,'deg_out':deg_out,'str_in':str_in,'str_out':str_out,
        'NSin_NDin': NSin_NDin, 'NSout_NDout':NSout_NDout,
        "dyad_census":dyad_census        
        }    """
    return r    

def ratio_ns_nd(g,mode='in'):
    """
    see [Barigozzi et al.2009] II.D., NS_in/ND_in

    Parameters:
    -------------
    g: igraph.Graph

    mode: str
    'in' or 'out'

    Outputs:
    --------
    x: array
    NS_{in/out}/ND_{in/out}
    """
    wei = np.array(g.strength(mode=mode,weights='weight'))
    wei_sum = np.sum(wei)
    if mode=='in':
        deg = np.array(g.indegree())
    elif mode=='out':
        deg = np.array(g.outdegree())
    else: raise ValueError('unknown mode')
    return wei/(wei_sum*  deg )

def fit_UBCM_igraph(mplex):
    """
    fit a UBCM for each layer independently
    """
    d_pipj = {}
    for layer,g in mplex.items() :
        # get degree sequence
        if g.is_directed():
            g.to_undirected()
        k = g.degree()
        k_ar=np.array( k ,dtype=np.int32 ) 
        if np.any(k_ar>=g.vcount()): raise ValueError('invalid degree')
        # solve for Pij under UBCM model
        sol = solve_eq_UBCM(k_ar)	
        if sol.success:
            P = pij_UBCM(sol.x)
            d_pipj[layer] = P
        else: warnings.warn('layer {} : solve_eq_UBCM did not converge'.format(layer)) 
    return d_pipj

def fit_UWCM_igraph(mplex):
    """
    fit a UWCM for each layer independently
    """
    d_pipj = {}
    for layer,g in mplex.items() :
        # get strength sequence
        if g.is_directed():
            g.to_undirected()
        s = np.array(g.strength(weights='weight'))
        # solve for Pij under UBCM model    
        sol = solve_eq_UWCM(s)
        if sol.success:
            P = pij_UWCM(sol.x)
            d_pipj[layer] = P
        else: warnings.warn('layer {} : solve_eq_UWCM did not converge'.format(layer))     
    return d_pipj


def link_overlap_pairwise_igraph(g1,g2,binary=True, directed=False):
    """
    compute link overlap between two graphs
    
    compute $ \sum_{i<j} min{a_{ij}^\alpha a_{ij}^\beta} $
    """
    count = 0 
    for (idx1,idx2) in g1.get_edgelist():
        w1 = g1[idx1,idx2]
        w2 = g2[idx1,idx2]
        m = min(w1,w2)
        if binary and m>0: m=1
        count += m
    return count

def link_overlap_pairwise_rescaled_igraph(G1,G2,P1,P2):
    """
    Direct application of the formula in  [Gem18] (1.52)
    https://igraph.org/python/doc/igraph.GraphBase-class.html#get_adjacency
    """
    W1 = G1.get_adjacency(attribute='weight') 
    W2 = G2.get_adjacency(attribute='weight') 
    W1 = np.tril(np.array(W1.data),-1)
    W2 = np.tril(np.array(W2.data),-1)
    min_W = np.minimum(W1,W2)
    P1P2 = P1*P2 
    avg = P1P2/ (1.-P1P2)
    numerator = 2 * np.sum( min_W  - avg   ) 
    denominator= np.sum( W1+W2 - 2*avg ) 
    return numerator/denominator
    
def link_overlap_rescaled_igraph(mplex,d_pipj):
    """
    """
    layers = mplex.keys()
    lo_resc = {}
    for layer_comb in itertools.combinations(layers, 2):    
        l_i,l_j = layer_comb    
        lo_resc[ layer_comb] = link_overlap_pairwise_rescaled_igraph(mplex[l_i],mplex[l_j],
                                    d_pipj[l_i],d_pipj[l_j])    
    return lo_resc   
        
#######################################################
# NX FUNCTIONS

#######################################################
# PLOT FUNCTIONS

def plot_overlap_mat(d1,d2, lab=['0','1']):
    """
    compare barplots of overlap
    """
    mat1 = overlap2mat(d1)
    mat2 = overlap2mat(d2)
    plt.subplot(2,1,1)
    plt.matshow(mat1,fignum=False, aspect="auto")
    plt.subplot(2,1,2)
    plt.matshow(mat2,fignum=False, aspect="auto")
    plt.show()



def plot_overlap_bar(d1,d2, lab=['0','1']):
    """
    compare barplots of overlap 
    https://matplotlib.org/gallery/lines_bars_and_markers/barchart.html#sphx-glr-gallery-lines-bars-and-markers-barchart-py

    REFS:
    [Bianconi 18] Fig. 7.2
    """
    d1_sorted = sorted(d1.items(), key=lambda kv: kv[1], reverse=True)  
    labels = [i[0] for i in d1_sorted]
    val1 = [ d1[lab] for lab in labels]
    val2 = [ d2[lab] for lab in labels]
    #
    x = np.arange(len(labels))  # the label locations
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width/2, val1, width, label=lab[0])
    rects2 = ax.bar(x + width/2, val2, width, label=lab[1])

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Scores')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()
    plt.show()
   
