# http://edgar.jrc.ec.europa.eu
# cartopy:  https://scitools.org.uk/cartopy/docs/latest/
#     install: sudo apt-get install python3-dev proj-bin libproj-dev libgeos-dev 
#              python3 -m pip install netcdf4 --user
#              python3 -m  pip install --user cartopy
#     netcdf: https://scitools.org.uk/cartopy/docs/v0.13/matplotlib/advanced_plotting.html
# (basemap: deprecated
# (https://matplotlib.org/basemap/
# (examples: https://jakevdp.github.io/PythonDataScienceHandbook/04.13-geographic-data-with-basemap.html
# (          https://rabernat.github.io/research_computing/intro-to-basemap.html
# (install basemap: https://github.com/matplotlib/basemap

# from netcdf doc
#from scipy.io import netcdf
#f = netcdf.netcdf_file(fname, 'r')
#print(f.history)

#time = f.variables['time']
#print(time.units)
#print(time.shape)
#print(time[-1])

# ---------------------------------------------------------
# PLOT EDGAR MAP 
# https://scitools.org.uk/cartopy/docs/v0.13/matplotlib/advanced_plotting.html

import os
import matplotlib.pyplot as plt
from netCDF4 import Dataset as netcdf_dataset
import numpy as np

import cartopy
from cartopy import config
import cartopy.crs as ccrs
import cartopy.io.shapereader as shpreader

# get the path of the file. It can be found in the repo data directory.
#fname = os.path.join(config["repo_data_dir"],
#                     'netcdf', 'HadISST1_SST_update.nc'
#                     )
fname ='/home/aurelien/local/data/MRIO/edgar/v432_CO2_excl_short-cycle_org_C_2012_IPCC_1A1a.0.1x0.1.nc' # 'simple.nc'

dataset = netcdf_dataset(fname)
#sst = dataset.variables['sst'][0, :, :]  
CO2 = dataset.variables['emi_co2'][:,:]
lats = dataset.variables['lat'][:]
lons = dataset.variables['lon'][:]

def test_plot(lons, lats, x):
	"""
	just plot the map 
	"""
	ax = plt.axes(projection=ccrs.PlateCarree())
	#ax.set_extent([-150, 60, -25, 60])
	ax.set_extent([0, 60, -25, 60])

	plt.contourf(lons, lats, x, 60,
				 transform=ccrs.PlateCarree())
	ax.coastlines()
	plt.show()

def test_plot_one_country():
	"""
	plot only one country
	https://gis.stackexchange.com/questions/88209/python-mapping-in-matplotlib-cartopy-color-one-country
	"""
	ax = plt.axes(projection=ccrs.PlateCarree())
	ax.add_feature(cartopy.feature.OCEAN)
	ax.set_extent([-150, 60, -25, 60])
	
	shpfilename = shpreader.natural_earth(resolution='110m',
	                                      category='cultural',
	                                      name='admin_0_countries')
	reader = shpreader.Reader(shpfilename)
	countries = reader.records()
	
	for country in countries:
		if country.attributes['ADM0_A3'] == 'USA':
			ax.add_geometries(country.geometry, ccrs.PlateCarree(),
								facecolor=(0, 0, 1),
								label=country.attributes['ADM0_A3'])
		else:
			ax.add_geometries(country.geometry, ccrs.PlateCarree(),
							  facecolor=(0, 1, 0),
							  label=country.attributes['ADM0_A3'])	
	plt.show()

def test_plot_by_country(lons, lats, x):
	"""
	plot only the points in a given country
	after https://gis.stackexchange.com/questions/88209/python-mapping-in-matplotlib-cartopy-color-one-country
	"""
	import shapely.geometry
	ax = plt.axes(projection=ccrs.PlateCarree())
	ax.add_feature(cartopy.feature.OCEAN)
	ax.set_extent([-150, 60, -25, 60])
	
	shpfilename = shpreader.natural_earth(resolution='110m',
	                                      category='cultural',
	                                      name='admin_0_countries')
	reader = shpreader.Reader(shpfilename)
	countries = reader.records()
	
	for country in countries:
		if country.attributes['ADM0_A3'] == 'USA':
			ax.add_geometries(country.geometry, ccrs.PlateCarree(),
								facecolor=(0, 0, 1),
									label=country.attributes['ADM0_A3'])
		# HERE look for points in lats,long that are contained by country
		#https://gis.stackexchange.com/questions/84114/shapely-unable-to-tell-if-polygon-contains-point
		#point = point = shapely.geometry.Point(lat,lng)
			point = shapely.geometry.Point(-100,40) #should be inside US
			print(country.geometry.contains(point))
			point = shapely.geometry.Point(0,0) #should be outside US
			print(country.geometry.contains(point))
		# TODO: country.geometry is shapely.geometry.multipolygon.MultiPolygon 	
		#       https://shapely.readthedocs.io/en/latest/manual.html
		# https://shapely.readthedocs.io/en/latest/manual.html#MultiPolygon
		
	plt.show()

# ----------------------------------------------------------
# FETCH EDGAR MAPS ??
# from https://github.com/openclimatedata/edgar-co2-emissions/blob/master/scripts/process.py
import os
import pandas as pd

from pathlib import Path


root = Path(__file__).parents[1]


total_emissions = pd.read_csv(
    root / "archive/EDGARv432_FT2016_CO2_total_emissions_1970-2016.csv",
    skiprows=5
)

assert total_emissions.substance.unique() == "CO2"
total_emissions = total_emissions.drop("substance", axis=1)

total_emissions = total_emissions.rename(columns={
    "ISO_CODE": "Code",
    "ISO_NAME": "Name",
    "sector": "Sector"
})

total_emissions = total_emissions.melt(
    id_vars=["Code", "Name", "Sector"],
    var_name="Year",
    value_name="Emissions"
)

non_standard = [i for i in total_emissions.Code.unique() if len(i) > 3]
print("Non standard country codes:")
print(non_standard)

total_emissions.to_csv(
    "data/edgar-co2-emissions.csv",
    encoding="UTF-8",
    index=False
)
